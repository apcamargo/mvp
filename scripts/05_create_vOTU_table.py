#!/usr/bin/env python3

import argparse
import os
import glob
import pandas as pd
from functools import reduce
import numpy as np

# create an ArgumentParser object to parse command-line arguments
parser = argparse.ArgumentParser(description='Module 05: Create vOTU tables based on read mapping CoverM files, and generate a final summary report.')

# define command-line arguments
parser.add_argument('-i', '--input', help='Path to your working directory.', required=True)
parser.add_argument('-m', '--metadata', help='Input metadata containing sample names, nucleotide file paths, and read files.', required=True)
parser.add_argument('--covered-fraction', nargs='+', type=float, default=[0.1, 0.5, 0.9], help='minimum covered fraction for filtering')
parser.add_argument('--normalization', type=str, default='RPKM', choices=['RPKM', 'FPKM'], help='Metrics to normalize')
parser.add_argument('--filtration', type=str, default='conservative', choices=['relaxed', 'conservative'], help='Filtration level ("relaxed" or "conservative"). Default = conservative')
parser.add_argument('--completeness', dest='completeness', metavar='COMPLETENESS', type=int, default=0, help='the minimum completeness value required to include a virus prediction (default = 0)')
parser.add_argument('--viral-min-genes', dest='viral_min_genes', metavar='VIRAL_MIN_GENES', type=int, default=0, help='the minimum number of viral genes required to include a virus prediction (default = 0)')
parser.add_argument('--viral-min-length', dest='viral_min_length', metavar='VIRAL_MIN_LENGTH', type=int, default=0, help='the minimum length required to include a predicted virus (default = 0)')
parser.add_argument('--host-viral-genes-ratio', dest='host_viral_genes_ratio', metavar='HOST_VIRAL_GENES_RATIO', type=float, default=1.0, help='the maximum ratio of host genes to viral genes required to include a virus prediction (default = 1)')

# parse command-line arguments
args = parser.parse_args()

# Combine all CoverM CSV files into one
read_mapping_dir = os.path.join(args.input, '04_READ_MAPPING', '*', '*_CoverM.csv')
all_coverm_csv = glob.glob(read_mapping_dir, recursive=True)
dfs = []

for filename in all_coverm_csv:
    df = pd.read_csv(filename, usecols=['Sample', 'Contig', 'Covered Fraction', 'Length', args.normalization]).drop_duplicates(subset=['Sample', 'Contig', 'Length'], keep='last').pivot(index=['Contig', 'Length'], columns='Sample')
    df.columns = ['_'.join(str(col) for col in cols).strip() for cols in df.columns.values]
    dfs.append(df)

df_coverm_merged = reduce(lambda left, right: pd.merge(left, right, on=['Contig', 'Length'], how='outer'), dfs).reset_index()

# read the quality summary TXT file
quality_summary_concatenated_df_path = os.path.join(args.input, '03_CLUSTERING', 'All_Sample_Genomad_CheckV_Virus_Sequences_Summary.tsv')
merged_quality_summary_concatenated_df = pd.read_csv(quality_summary_concatenated_df_path, sep='\t')

# merge the CoverM and TSV dataframes
df_merged = pd.merge(merged_quality_summary_concatenated_df, df_coverm_merged, left_on='virus_id', right_on='Contig', how='right')
df_merged = df_merged.loc[:, ~df_merged.columns.duplicated(keep='last')]
df_merged['completeness'].fillna(0, inplace=True) ## When no completeness, set it at 0 so that the sequence does not get filtered out (except when minimum completeness is required)
# Remove columns that are in the columns_to_exclude list
columns_to_exclude = ['Contig', 'Length']
df_merged = df_merged.drop(columns=columns_to_exclude)

# Save the merged DataFrame as a CSV file in the output folder
output_file_path = os.path.join(args.input, '05_VOTU_TABLES', f'Unfiltered_{args.normalization}_vOTU_Table.csv')
df_merged.to_csv(output_file_path, index=False)

# Apply relaxed filtration criteria
df_filtered = df_merged.loc[(df_merged['viral_genes'] >= args.viral_min_genes)]
df_filtered = df_merged.loc[df_merged['host_genes']/df_merged['viral_genes'] <= args.host_viral_genes_ratio]

# Apply the new filtration logic if args.filtration is conservative
if args.filtration == 'conservative':
    df_filtered = df_merged.loc[
        (((df_merged['completeness_method'].str.contains('AAI-based')) | (df_merged['completeness_method'].str.contains('DTR'))) &
         (df_merged['checkv_quality'].isin(['High-quality', 'Medium-quality']))) | (df_merged['virus_length'] > 5000)]
    
# Save the merged DataFrame as a CSV file in the output folder
output_filtered_file_path = os.path.join(args.input, '05_VOTU_TABLES', f'Filtered_{args.filtration}_{args.normalization}_vOTU_Table.csv')
df_filtered.to_csv(output_filtered_file_path, index=False)

# Define a function to filter and update RPKM columns based on covered fraction
def filter_and_update_RPKM(row, cf, covered_fraction_cols, TPM_cols):
    for sample, cf_col in enumerate(covered_fraction_cols):
        cf_val = row[cf_col]
        if cf_val < cf:
            tpm_col = TPM_cols[sample]
            row[tpm_col] = 0
    return row

# Remove rows with Covered Fraction less than the specified value in each sample for each covered fraction value
df_filtered_list = []

for cf in args.covered_fraction:
    # Make a copy of df_merged so that we don't modify it directly
    df_temp = df_filtered.copy()
    covered_fraction_cols = df_temp.filter(like='Covered Fraction').columns
    TPM_cols = df_temp.filter(like=args.normalization).columns

    # Apply the filter_and_update_RPKM function to each row
    df_temp = df_temp.apply(filter_and_update_RPKM, args=(cf, covered_fraction_cols, TPM_cols), axis=1)
    
    # Filter out rows where all RPKM values are 0
    df_temp = df_temp[~(df_temp[TPM_cols] == 0).all(axis=1)]
    df_temp = df_temp.loc[:, ~df_temp.columns.str.contains('Covered Fraction')]
    df_filtered_list.append(df_temp)

# Save the filtered dataframes
for i, cf in enumerate(args.covered_fraction):
    completeness_suffix = f'_Completeness_{args.completeness}' if args.completeness else ''
    output_file = os.path.join(args.input, '05_VOTU_TABLES', f'Filtered_{args.filtration}_HC_{cf}{completeness_suffix}_{args.normalization}_Representative_vOTU_Table.csv')
    df_filtered_list[i].to_csv(output_file, index=False)
    print(f'Filtered_{args.filtration}_HC_{cf}{completeness_suffix}_{args.normalization}_Representative_vOTU_Table.csv' + ' saved in ' + args.input + '/05_VOTU_TABLES')

# Calculate summary statistics
num_vOTUs = len(df_filtered)
num_proviruses = (df_filtered['provirus'].str.contains('Yes', case=False)).sum()
mean_length = df_filtered['virus_length'].mean()
max_length = df_filtered['virus_length'].max()
min_length = df_filtered['virus_length'].min()
quality_counts = df_filtered['checkv_quality'].value_counts()

# Add columns and counts for RPKM statistics
rpkm_columns = [col for col in df_filtered.columns if col.startswith('RPKM_')]
rpkm_virus_counts = [(col, (df_filtered[col] > 0).sum()) for col in rpkm_columns]
# Sort RPKM counts in descending order
rpkm_virus_counts.sort(key=lambda x: x[1], reverse=True)

# Count occurrences of each realm in the second level of the taxonomy column
taxa_levels = df_filtered['taxonomy'].str.split(';')
second_level_taxa = taxa_levels.apply(lambda x: x[1] if len(x) > 1 else 'Unclassified/others')
taxa_counts = second_level_taxa.value_counts()
# Calculate the percentage of each realm
taxa_percentages = (taxa_counts / num_vOTUs) * 100
# Prepare the summary report content
taxa_summary = "\n".join([f"{realm}: {count} ({percentage:.1f}%)" for realm, count, percentage in zip(taxa_counts.index, taxa_counts.values, taxa_percentages.values)])

# Prepare the summary report content for top classes
taxa_levels = df_filtered['taxonomy'].str.split(';').apply(lambda levels: levels + [''] * (5 - len(levels)))
fifth_level_classes = taxa_levels.apply(lambda x: x[4])
filtered_classes = fifth_level_classes[(fifth_level_classes != '') & (fifth_level_classes != 'Unclassified')]
class_counts = filtered_classes.value_counts()
top_10_classes = class_counts.nlargest(10)
top_classes_summary = "\n".join([f"{cls}: {count}" for cls, count in top_10_classes.items()])

# Define the path to the summary report created in Module 01
summary_report_path_module_03 = os.path.join(args.input, '03_CLUSTERING', 'MVP_03_Summary_Report.txt')

# Open the summary report created in Module 01 and read its content
with open(summary_report_path_module_03, 'r') as module_03_summary_report:
    module_03_summary_content = module_03_summary_report.read()

# Define the additional lines you want to add to the summary report
module_05_lines_to_add = """
****************************************************************************
******************               MODULE 05                ******************
****************************************************************************
"""

# Create a dictionary to hold argument descriptions and their default values
argument_defaults = {
    '--input': args.input,
    '--metadata': args.metadata,
    '--covered-fraction': args.covered_fraction,
    '--normalization': args.normalization,
    '--filtration': args.filtration,
    '--completeness': args.completeness,
    '--viral-min-genes': args.viral_min_genes,
    '--viral_min_length': args.viral_min_length,
    '--host-viral-genes-ratio': args.host_viral_genes_ratio}

# Write a summary line with script arguments and their default values
summary_line = "05_create_votu_table.py"
for arg, default in argument_defaults.items():
    if default is not None:
        summary_line += f" {arg} {default}"

# Combine the content of Module 01 and Module 02 lines
complete_summary_content = module_03_summary_content + module_05_lines_to_add + summary_line

complete_summary_content +=f"""
\nSummary Report after both clustering and filtration
--------------------------
Number of vOTUs: {num_vOTUs}, including {num_proviruses} proviruses
Mean vOTUs Length: {mean_length:.2f}
Max vOTUs Length: {max_length}
Min vOTUs Length: {min_length}

vOTUs CheckV quality summary:
Low Quality: {quality_counts.get('Low-quality', 0)}
Medium Quality: {quality_counts.get('Medium-quality', 0)}
High Quality: {quality_counts.get('High-quality', 0)}
Complete: {quality_counts.get('Complete', 0)}
Not determined: {quality_counts.get('Not-determined', 0)}

vOTUs taxonomy summary:
{taxa_summary}

vOTUs Top 10 classes:
{top_classes_summary}

vOTUs Statistics per Sample:
"""
most_abundant_vOTUs = {}  # Dictionary to store most abundant vOTUs
for col, count in rpkm_virus_counts:
    sample_name = col.replace('RPKM_', '')
    max_idx = df_merged[col].idxmax()
    vOTU_id = df_merged.loc[max_idx]['virus_id']
    taxonomy = df_merged.loc[max_idx]['taxonomy'].split(';')
    
    # Get the Class (fifth level of the taxonomy), or use 'Unclassified' if not available
    vOTU_class = taxonomy[4] if len(taxonomy) >= 5 else 'Unclassified'
    
    most_abundant_vOTUs[sample_name] = {'vOTU_id': vOTU_id, 'vOTU_class': vOTU_class}  # Store most abundant vOTU and its Class
    complete_summary_content += f"{sample_name}: {count} vOTUs | Most Abundant vOTU: {vOTU_id} ({vOTU_class})\n"

summary_report_path_module_05 = os.path.join(args.input, '05_VOTU_TABLES', 'MVP_05_Summary_Report.txt')

# Write the combined content to the new summary report for Module 02
with open(summary_report_path_module_05, 'w') as module_05_summary_report:
    module_05_summary_report.write(complete_summary_content)

print("\nSummary report for Module 05 has been created: ", summary_report_path_module_05)

message1 = "\n\033[1mModule 05 finished: vOTU tables and summary report generated!\033[0m\n"
message2 = f"Output vOTU tables and summary report file have been saved in the {args.input}05_VOTU_TABLES directory."
message3 = "\n\033[1mWe hope you enjoyed using MVP script, and you can now explore your data!\033[0m\n"
line_of_stars = '*' * len(message2)
print()
print(line_of_stars)
print(message1)
print(message2)
print(message3)
print(line_of_stars)
print()

print("Please don't forget to cite the MVP script!\n")