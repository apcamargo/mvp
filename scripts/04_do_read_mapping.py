import argparse
import os
import subprocess
import glob
import pandas as pd
from functools import reduce

# create an ArgumentParser object to parse command-line arguments
parser = argparse.ArgumentParser(description='Module 04: Generate all files needed for the read mapping and create coverM files with coverage information.')

# define command-line arguments
parser.add_argument('-i', '--input', help='Path to your working directory.', required=True)
parser.add_argument('-m', '--metadata', help='Input metadata containing sample names, nucleotide file paths, and read files.', required=True)
parser.add_argument('--sample-group', help='Specific sample number(s) to run the script on (can be a comma-separated list: 1,2,6 for example). By default, MVP processes all datasets listed in the metadata file one after the other.', type=str, default='')
parser.add_argument('--read-type', type=str, default='short', choices=['short', 'long'], help='Data types (e.g. short vs long reads). Default = short')
parser.add_argument('--delete-files', action='store_true', help='flag to delete unwanted files')
parser.add_argument('--threads', type=int, default=1, help='Number of threads to use (default = 1)')

# parse command-line arguments
args = parser.parse_args()

# read metadata file and map FASTQ files
metadata = pd.read_csv(args.metadata, delimiter='\t')

# Initialize a counter for processed samples
processed_samples_count = 0

# Process rows based on specific sample numbers or all rows
specific_sample_numbers = []
if args.sample_group:
    specific_sample_numbers = [int(num) for num in args.sample_group.split(',') if num.strip()]

for row_index, (_, row) in enumerate(metadata.iterrows(), start=1):
    input_assembly_file = row['Assembly_Path']
    sample_number = row['Sample_number']
    sample_name = row['Sample']

    # Check if the current sample number is in the list of specific sample numbers
    if specific_sample_numbers and sample_number not in specific_sample_numbers:
        continue # Skip to the next iteration

    # Create directories for the current sample in 01_GENOMAD and 02_CHECK_V
    read_mapping_sample_directory = os.path.join(args.input, '04_READ_MAPPING', str(sample_name))
    os.makedirs(read_mapping_sample_directory, exist_ok=True)

    input_fastq = row['Read_Path']
    sample_name = row['Sample']
    reference_path = os.path.join(args.input, '04_READ_MAPPING', 'reference')
    output_sam = os.path.join(read_mapping_sample_directory, str(sample_name) + '.sam')
    output_bam = os.path.join(read_mapping_sample_directory, str(sample_name) + '.bam')
    sorted_bam = os.path.join(read_mapping_sample_directory, str(sample_name) + '_sorted.bam')
    output_coverm = os.path.join(read_mapping_sample_directory, os.path.basename(sorted_bam).replace('sorted.bam', 'CoverM.csv'))

    if os.path.exists(sorted_bam) and os.path.exists(output_coverm):
        print(f"Skipping mapping for {sample_name}, sorted BAM and coverM files already exists")
    else:
        print(f"\nStarting the read mapping of {sample_name}...\n")

        # If args.read_type is "short" or not provided, use Bowtie2 command
        if args.read_type == 'short' or not args.read_type:

            # Check if "R1" is in the input_fastq filename
            if "R1" in input_fastq:
                # Create the corresponding R2 input filename
                input_fastq_r2 = input_fastq[:input_fastq.rfind("R1")] + "R2" + input_fastq[input_fastq.rfind("R1")+2:]
                # Check if the R2 input file exists
                if os.path.exists(input_fastq_r2) and not os.path.exists(output_sam):
                    print("\nRun Bowtie2: use paired fastq files as input against database created in the previous step to create SAM files.\n")
                    subprocess.run(['bowtie2', '-x', reference_path, '-1', input_fastq, '-2', input_fastq_r2, '-S', output_sam, '-p', str(args.threads), '--no-unal'])
                else:
                    print(f"Skipping mapping for {sample_name}, R2 input file '{input_fastq_r2}' not found")
            else:
                if os.path.exists(output_sam):
                    print(f"Skipping Bowtie2: for {sample_name}, output SAM file already exists")
                else:
                    print("Run Bowtie2: use fastq file as input against database created in last step to create SAM files.\n")
                    subprocess.run(['bowtie2', '-x', reference_path, '-U', input_fastq, '-S', output_sam, '-p', str(args.threads), '--no-unal'])
        
        # If args.read_type is "long", use minimap2 command
        elif args.read_type == 'long':
            
            # Check if "R1" is in the input_fastq filename
            if "R1" in input_fastq:
                # Create the corresponding R2 input filename
                input_fastq_r2 = input_fastq[:input_fastq.rfind("R1")] + "R2" + input_fastq[input_fastq.rfind("R1")+2:]
                # Check if the R2 input file exists
                if os.path.exists(input_fastq_r2) and not os.path.exists(output_sam):
                    print("\nRun minimap2: use paired fastq files as input against database created in the previous step to create SAM files.\n")
                    subprocess.run(['minimap2', '-a', reference_path, input_fastq, input_fastq_r2, '--sam-hit-only', '-t', str(args.threads), '>', output_sam])
                else:
                    print(f"Skipping mapping for {sample_name}, R2 input file '{input_fastq_r2}' not found")
            else:
                if os.path.exists(output_sam):
                    print(f"Skipping Bowtie2: for {sample_name}, output SAM file already exists")
                else:
                    print("Run Bowtie2: use fastq file as input against database created in last step to create SAM files.\n")
                    subprocess.run(['minimap2', '-a', reference_path, input_fastq, '--sam-hit-only', '-t', str(args.threads), '>', output_sam])

        if os.path.exists(output_bam):
            print(f"Skipping to convert SAM file to BAM for {sample_name}, output BAM file already exists")
        else:
            print("\nRun samtool view to convert SAM files to BAM.\n")
            subprocess.run(['samtools', 'view', '-S', '-b', output_sam, '-o', output_bam, '-@', str(args.threads)])
                
        print("Run samtool sort to sort BAM files.\n")
        subprocess.run(['samtools', 'sort', output_bam, '-o', sorted_bam, '-@', str(args.threads)])

        print("\nRun CoverM: use sorted BAM files to calculate read coverage and relative abundance of representative viral sequences.\n")
        subprocess.run(['coverm', 'contig', '-b', sorted_bam, '-m', 'mean', 'trimmed_mean', 'covered_bases', 'covered_fraction', 'variance', 'length', 'count', 'reads_per_base', 'rpkm', 'tpm', '-o', output_coverm, '--output-format', 'sparse'])
        
        # Read in the CoverM output file as a DataFrame
        df = pd.read_csv(output_coverm, sep='\t')
        # Replace '_sorted' with an empty string in the 'Sample' column
        df['Sample'] = df['Sample'].str.replace('_sorted', '')
        # Write the modified DataFrame back to the output file
        df.to_csv(output_coverm, index=False)
    
    # delete unwanted files
    read_mapping_path = os.path.join(args.input, '04_READ_MAPPING')
    if args.delete_files:
        allowed_extensions = ('.bt2', '_sorted.bam', '.csv')
        for file in os.listdir(read_mapping_path):
            if not file.endswith(allowed_extensions):
                os.remove(os.path.join(read_mapping_path, file))

    # Increment the processed_samples_count for each processed sample
    processed_samples_count += 1

    # Define the path to the summary report created in Module 01
    summary_report_path_module_03 = os.path.join(args.input, '03_CLUSTERING', 'MVP_03_Summary_Report.txt')

    # Open the summary report created in Module 01 and read its content
    with open(summary_report_path_module_03, 'r') as module_03_summary_report:
        module_03_summary_content = module_03_summary_report.read()

# Define the additional lines you want to add to the summary report
    module_04_lines_to_add = """
****************************************************************************
******************               MODULE 04                ******************
****************************************************************************
"""

    # Create a dictionary to hold argument descriptions and their default values
    argument_defaults = {
        '--input': args.input,
        '--metadata': args.metadata,
        '--sample-group': args.sample_group,
        '--delete-files': args.delete_files,
        '--threads': args.threads}

    # Write a summary line with script arguments and their default values
    summary_line = "04_do_read_mapping.py"
    for arg, default in argument_defaults.items():
        if default is not None:
            summary_line += f" {arg} {default}"

    # Combine the content of Module 01 and Module 02 lines
    complete_summary_content = module_03_summary_content + module_04_lines_to_add + summary_line

    # Specify the path for the new MVP_Summary_Report.txt in '01_GENOMAD' directory
    summary_report_path_module_04 = os.path.join(read_mapping_sample_directory, str(sample_name) + '_MVP_04_Summary_Report.txt')

    # Write the combined content to the new summary report for Module 02
    with open(summary_report_path_module_04, 'w') as module_04_summary_report:
        module_04_summary_report.write(complete_summary_content)

    print("\nSummary report for Module 04 has been created: ", summary_report_path_module_04, " in ", read_mapping_sample_directory)

message1 = "\n\033[1mModule 04 finished: read mapping executed successfully!\033[0m\n"
message2 = f"Output files (sorted.bam and read mapping TXT files) have been saved in the {args.input}04_READ_MAPPING directories.\n"
message3 = f"SAM and BAM files deleted if --delete-files used."
message4 = f"Generated coverM.csv files will be used as inputs for Module 05 (creation of vOTU tables)."
message5 = "\n\033[1mYou can now proceed to the next step of the MVP script: Module 05!\033[0m\n"
line_of_stars = '*' * len(message2)
print()
print(line_of_stars)
print(message1)
print(message2)
print(message3)
print(message4)
print(message5)
print(line_of_stars)
print()

print("Please don't forget to cite the following softwares used by this module:")
print("- Langmead B, Salzberg S. Fast gapped-read alignment with Bowtie 2. Nature Methods. 2012, 9:357-359\n")
print("- Li, H., B. Handsaker, A. Wysoker, T. Fennell, J. Ruan, N. Homer, G. Marth, G. Abecasis, R. Durbin, and 1000 Genome Project Data Processing Subgroup. The Sequence Alignment/Map Format and SAMtools. Bioinformatics 25, no. 16 2009: 2078–79.\n")
print("- Li H. New strategies to improve minimap2 alignment accuracy. Bioinformatics. 2021, 37(23):4572–4574. https://academic.oup.com/bioinformatics/article/37/23/4572/6384570?login=true\n")
print("- https://github.com/wwood/CoverM\n")