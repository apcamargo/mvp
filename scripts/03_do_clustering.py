import argparse
import os
import subprocess
import pandas as pd
import glob
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord

# Parse command line arguments
parser = argparse.ArgumentParser(description='Module 03: Viral sequence clustering and reference preparation for the read mapping step.')
parser.add_argument('-i', '--input', help='Path to your working directory.', required=True)
parser.add_argument('-m', '--metadata', help='Input metadata containing sample names, nucleotide file paths, and read files.', required=True)
parser.add_argument('--min_ani', type=int, default=95, help='Minimum ANI value for clustering (default = 95)')
parser.add_argument('--min_tcov', type=int, default=85, help='Minimum coverage of the target sequence (default = 85)')
parser.add_argument('--min_qcov', type=int, default=0, help='Minimum coverage of the query sequence (default = 0)')
parser.add_argument('--read-type', type=str, default='short', choices=['short', 'long'], help='Sequencing data type (e.g. short vs long reads). Default = short')
parser.add_argument('--threads', type=int, default=1, help='Number of threads to use (default = 1)')

args = parser.parse_args()

# Load metadata DataFrame
metadata_df = pd.read_csv(args.metadata, sep='\t')

# Get a list of all input FASTA files within the specified directories
input_files = []
input_pattern = os.path.join(args.input, '02_CHECK_V', '*', '*_virus_provirus.fna')
input_files.extend(glob.glob(input_pattern, recursive=True))

# Concatenate the input FASTA files
output_file = os.path.join(args.input, '03_CLUSTERING', 'All_Sample_Genomad_CheckV_Virus_Sequences.fna')
with open(output_file, 'w') as outfile:
    for fasta_file in input_files:
        with open(fasta_file, 'r') as infile:
            outfile.write(infile.read())

# Get the directory of the current script
script_dir = os.path.dirname(os.path.abspath(__file__))
# Construct the path to 03_anicalc.py and 03_aniclust.py
anicalc_path = os.path.join(script_dir, '03_anicalc.py')
aniclust_path = os.path.join(script_dir, '03_aniclust.py')

# Run makeblastdb on the concatenated FASTA file
blastdb_output = os.path.join(args.input, '03_CLUSTERING', 'All_Sample_Genomad_CheckV_Virus_Representative_Sequences_BLAST_DB')
subprocess.run(['makeblastdb', '-in', output_file, '-dbtype', 'nucl', '-out', blastdb_output])

# Run blastn on the concatenated FASTA file
blastn_output = os.path.join(args.input, '03_CLUSTERING', 'All_Sample_Genomad_CheckV_Virus_Representative_Sequences_Clustering.tsv')
subprocess.run(['blastn', '-query', output_file, '-db', blastdb_output, '-outfmt', '6 std qlen slen', '-max_target_seqs', '10000', '-out', blastn_output, '-num_threads', str(args.threads)])

# Run anicalc.py on the blastn output
ani_output = os.path.join(args.input, '03_CLUSTERING', 'All_Sample_Genomad_CheckV_Virus_Representative_Sequences_Clustering_ANI.tsv')
subprocess.run(['python', anicalc_path, '-i', blastn_output, '-o', ani_output])

# Run aniclust.py on the anicalc.py output
cluster_output = os.path.join(args.input, '03_CLUSTERING', 'All_Sample_Genomad_CheckV_Virus_Representative_Sequences_Clustering_ANI_Clusters.tsv')
subprocess.run(['python', aniclust_path, '--fna', output_file, '--ani', ani_output, '--out', cluster_output, '--min_ani', str(args.min_ani), '--min_tcov', str(args.min_tcov), '--min_qcov', str(args.min_qcov)])

# Add column names to the output file
df = pd.read_csv(cluster_output, sep='\t', header=None)
df.columns = ['Representative_Sequence', 'Sequences']
contig_ids_to_keep = set(df['Representative_Sequence'].tolist())
df.to_csv(cluster_output, sep='\t', index=False)

# Search for files ending with '_Genomad_CheckV_Virus_Proviruses_Quality_Summary.tsv'
quality_summary_files = []
quality_summary_pattern = os.path.join(args.input, '02_CHECK_V', '*', '*_Genomad_CheckV_Virus_Proviruses_Quality_Summary.tsv')
quality_summary_files.extend(glob.glob(quality_summary_pattern))

# Initialize an empty DataFrame to store the concatenated data
quality_summary_concatenated_df = pd.DataFrame()

# Loop through each quality summary file and concatenate the data
for quality_summary_file in quality_summary_files:
    quality_summary_df = pd.read_csv(quality_summary_file, sep='\t')
    quality_summary_concatenated_df = pd.concat([quality_summary_concatenated_df, quality_summary_df], ignore_index=True)

# Save the quality_summary_concatenated_df DataFrame to a new file
quality_summary_concatenated_file = os.path.join(args.input, '03_CLUSTERING', 'All_Sample_Genomad_CheckV_Virus_Sequences_Summary.tsv')
quality_summary_concatenated_df.to_csv(quality_summary_concatenated_file, sep='\t', index=False)

merged_quality_summary_concatenated_df = pd.merge(df, quality_summary_concatenated_df, left_on='Representative_Sequence',right_on='virus_id', how='left')
columns_to_exclude = ['virus_id']  # Specify the columns to exclude
merged_quality_summary_concatenated_df = merged_quality_summary_concatenated_df.drop(columns=columns_to_exclude)

# Save the merged DataFrame to a new file
merged_output_file = os.path.join(args.input, '03_CLUSTERING', 'All_Sample_Genomad_CheckV_Virus_Representative_Sequences_Clustering_ANI_Clusters_Summary.tsv')
merged_quality_summary_concatenated_df.to_csv(merged_output_file, sep='\t', index=False)


# Calculate summary statistics before both clustering and  filtration
num_viruses = len(quality_summary_concatenated_df)
num_proviruses = (quality_summary_concatenated_df['provirus'].str.contains('Yes', case=False)).sum()
mean_virus_length = quality_summary_concatenated_df['virus_length'].mean()
max_virus_length = quality_summary_concatenated_df['virus_length'].max()
min_virus_length = quality_summary_concatenated_df['virus_length'].min()
virus_quality_counts = quality_summary_concatenated_df['checkv_quality'].value_counts()

# Count occurrences of each realm in the second level of the taxonomy column
virus_taxa_levels = quality_summary_concatenated_df['taxonomy'].str.split(';')
virus_second_level_taxa = virus_taxa_levels.apply(lambda x: x[1] if len(x) > 1 else 'Unclassified/others')
virus_taxa_counts = virus_second_level_taxa.value_counts()
# Calculate the percentage of each realm
virus_taxa_percentages = (virus_taxa_counts / num_viruses) * 100
# Prepare the summary report content
virus_taxa_summary = "\n".join([f"{realm}: {count} ({percentage:.1f}%)" for realm, count, percentage in zip(virus_taxa_counts.index, virus_taxa_counts.values, virus_taxa_percentages.values)])

# Prepare the summary report content for top classes
virus_taxa_levels = quality_summary_concatenated_df['taxonomy'].str.split(';').apply(lambda levels: levels + [''] * (5 - len(levels)))
virus_fifth_level_classes = virus_taxa_levels.apply(lambda x: x[4])
virus_filtered_classes = virus_fifth_level_classes[(virus_fifth_level_classes != '') & (virus_fifth_level_classes != 'Unclassified')]
virus_class_counts = virus_filtered_classes.value_counts()
virus_top_10_classes = virus_class_counts.nlargest(10)
virus_top_classes_summary = "\n".join([f"{cls}: {count}" for cls, count in virus_top_10_classes.items()])



# Calculate summary statistics after clustering and before filtration
num_vOTUs = len(merged_quality_summary_concatenated_df)
num_provotus = (merged_quality_summary_concatenated_df['provirus'].str.contains('Yes', case=False)).sum()
mean_length = merged_quality_summary_concatenated_df['virus_length'].mean()
max_length = merged_quality_summary_concatenated_df['virus_length'].max()
min_length = merged_quality_summary_concatenated_df['virus_length'].min()
quality_counts = merged_quality_summary_concatenated_df['checkv_quality'].value_counts()

# Count occurrences of each realm in the second level of the taxonomy column
taxa_levels = merged_quality_summary_concatenated_df['taxonomy'].str.split(';')
second_level_taxa = taxa_levels.apply(lambda x: x[1] if len(x) > 1 else 'Unclassified/others')
taxa_counts = second_level_taxa.value_counts()
# Calculate the percentage of each realm
taxa_percentages = (taxa_counts / num_vOTUs) * 100
# Prepare the summary report content
taxa_summary = "\n".join([f"{realm}: {count} ({percentage:.1f}%)" for realm, count, percentage in zip(taxa_counts.index, taxa_counts.values, taxa_percentages.values)])

# Prepare the summary report content for top classes
taxa_levels = merged_quality_summary_concatenated_df['taxonomy'].str.split(';').apply(lambda levels: levels + [''] * (5 - len(levels)))
fifth_level_classes = taxa_levels.apply(lambda x: x[4])
filtered_classes = fifth_level_classes[(fifth_level_classes != '') & (fifth_level_classes != 'Unclassified')]
class_counts = filtered_classes.value_counts()
top_10_classes = class_counts.nlargest(10)
top_classes_summary = "\n".join([f"{cls}: {count}" for cls, count in top_10_classes.items()])

# Define the path for the new summary report for Module 03
summary_report_path_module_03 = os.path.join(args.input, '03_CLUSTERING', 'MVP_03_Summary_Report.txt')

# Create a dictionary to hold argument descriptions and their default values
argument_defaults = {
    '--input': args.input,
    '--metadata': args.metadata,
    '--min_ani': args.min_ani,
    '--min_tcov': args.min_tcov,
    '--min_qcov': args.min_qcov,
    '--threads': args.threads}

# Write a summary line with script arguments and their default values
summary_line = "03_do_clustering.py"
for arg, default in argument_defaults.items():
    if default is not None:
        summary_line += f" {arg} {default}"

# Prepare the content for Module 03 summary report
module_03_summary_content = f"""
****************************************************************************
******************               MODULE 03                ******************
****************************************************************************

{summary_line}

Summary Report before both clustering and filtration
--------------------------
Number of Viruses: {num_viruses}, including {num_proviruses} proviruses
Mean Viruses Length: {mean_virus_length:.2f}
Max Viruses Length: {max_virus_length}
Min Viruses Length: {min_virus_length}

Viruses CheckV quality summary:
Low Quality: {virus_quality_counts.get('Low-quality', 0)}
Medium Quality: {virus_quality_counts.get('Medium-quality', 0)}
High Quality: {virus_quality_counts.get('High-quality', 0)}
Complete: {virus_quality_counts.get('Complete', 0)}
Not determined: {virus_quality_counts.get('Not-determined', 0)}

Viruses taxonomy summary:
{virus_taxa_summary}

Top 10 classes:
{virus_top_classes_summary}

Summary Report after clustering and before filtration
--------------------------
Number of vOTUs: {num_vOTUs}, including {num_provotus} proviruses
Mean vOTUs Length: {mean_length:.2f}
Max vOTUs Length: {max_length}
Min vOTUs Length: {min_length}

vOTUs CheckV quality summary:
Low Quality: {quality_counts.get('Low-quality', 0)}
Medium Quality: {quality_counts.get('Medium-quality', 0)}
High Quality: {quality_counts.get('High-quality', 0)}
Complete: {quality_counts.get('Complete', 0)}
Not determined: {quality_counts.get('Not-determined', 0)}

vOTUs taxonomy summary:
{taxa_summary}

Top 10 classes:
{top_classes_summary}

"""

# Write the content to the new summary report for Module 03
with open(summary_report_path_module_03, 'w') as module_03_summary_report:
    module_03_summary_report.write(module_03_summary_content)

print("\nSummary report for Module 03 has been created: ", summary_report_path_module_03)

# Find the directory containing the viruses and proviruses fasta files for this sample
fasta_lines = []
with open(output_file, 'r') as f:
    fasta_lines = f.readlines()

# Filter the concatenated file
representative_fasta_file = os.path.join(args.input, '03_CLUSTERING', 'All_Sample_Genomad_CheckV_Virus_Representative_Sequences.fna')
with open(representative_fasta_file, 'w') as f:
    for line in fasta_lines:
        if line.startswith('>'):
            contig_id = line.strip()[1:]
            if contig_id in contig_ids_to_keep:
                write_line = True
                f.write(line)
            else:
                write_line = False
        else:
            if write_line:
                f.write(line)

# Find path for '04_READ_MAPPING'
read_mapping_dir = os.path.join(args.input, '04_READ_MAPPING')

# If args.read_type is "short" or not provided, create a Bowtie2 index if it does not exist
reference_path = os.path.join(read_mapping_dir, 'reference')

if args.read_type == 'short' or not args.read_type:
    if not os.path.exists(reference_path + '.1.bt2'):
        print("\nRun bowtie2-build: use representative viral sequences to build bowtie database.\n")
        subprocess.run(['bowtie2-build', representative_fasta_file, reference_path])
    else:
        print(f"\nReference index 'reference' already exists in {read_mapping_dir}")
else:
    # If args.read_type is "long," apply minimap2 to create an index (assuming minimap2 is in your PATH)
    if not os.path.exists(reference_path + '.mmi'):
        print("\nRun minimap2 to create an index from representative viral sequences.\n")
        subprocess.run(['minimap2', '-d', reference_path, representative_fasta_file])
    else:
        print(f"\nReference index 'reference' already exists in {read_mapping_dir}")

functional_annotation_directory = os.path.join(args.input, '06_FUNCTIONAL_ANNOTATION')
os.makedirs(functional_annotation_directory, exist_ok=True)

# Create a dictionary to store sequences by sample_name
sequences_by_sample = {}

# Iterate over rows in the metadata DataFrame
for row_index, (_, row) in enumerate(metadata_df.iterrows(), start=1):
    sample_name = row['Sample']
    # Construct the path to the provirus protein file based on the sample name
    provirus_protein_file = os.path.join(args.input, '01_GENOMAD', str(sample_name), f'{sample_name}_Proviruses_Genomad_Output', 'proviruses_summary', 'proviruses_virus_proteins.faa')
    virus_protein_files = glob.glob(os.path.join(args.input, '01_GENOMAD', str(sample_name), f'{sample_name}_Viruses_Genomad_Output', f'{sample_name}*_summary', f'{sample_name}*_virus_proteins.faa'))

    # Check if the provirus protein file exists
    if os.path.exists(provirus_protein_file):
        # Load sequences from the provirus protein file
        provirus_sequences = SeqIO.to_dict(SeqIO.parse(provirus_protein_file, "fasta"))

        # Initialize a list to store sequences from virus_protein_files that are not similar to provirus_sequences
        non_similar_sequences = []

        # Iterate over virus protein files
        for virus_protein_file in virus_protein_files:
            # Load sequences from the virus protein file
            virus_sequences = SeqIO.to_dict(SeqIO.parse(virus_protein_file, "fasta"))

            # Find sequences from virus_sequences that are not similar to any sequences in provirus_sequences
            for seq_id, seq_record in virus_sequences.items():
                seq_content = str(seq_record.seq)
                if not any(seq_content in str(provirus_seq.seq) for provirus_seq in provirus_sequences.values()):
                    non_similar_sequences.append(seq_record)

        # Store the combined sequences from provirus_protein_file and non_similar_sequences
        combined_sequences = list(provirus_sequences.values()) + non_similar_sequences
        sequences_by_sample[sample_name] = combined_sequences
    else:
        # If provirus protein file is missing, use virus protein files as is
        combined_sequences = []
        for virus_protein_file in virus_protein_files:
            combined_sequences.extend(SeqIO.parse(virus_protein_file, "fasta"))
        sequences_by_sample[sample_name] = combined_sequences

# Concatenate all sequences for each sample into a single file
protein_file = os.path.join(args.input, '06_FUNCTIONAL_ANNOTATION', "All_Sample_Genomad_CheckV_Virus_Provirus_Sequences_proteins.faa")

with open(protein_file, "w") as output_handle:
    for sample_name, sequences in sequences_by_sample.items():
        SeqIO.write(sequences, output_handle, "fasta")

# Define the output file path
representative_protein_file = os.path.join(args.input, '06_FUNCTIONAL_ANNOTATION', "All_Sample_Genomad_CheckV_Virus_Provirus_Representative_Sequences_proteins.faa")

# Load the headers from the representative fasta file into a set for faster lookup
representative_headers = set(SeqIO.to_dict(SeqIO.parse(representative_fasta_file, "fasta")).keys())

# Open the input file containing all sequences
with open(representative_protein_file, "w") as output_handle:
    for sample_name, sequences in sequences_by_sample.items():
        for seq_record in sequences:
            header = seq_record.description
            # Check if any substring of the header is in the representative_headers set
            if any(substring in header for substring in representative_headers):
                SeqIO.write(seq_record, output_handle, "fasta")


# Get a list of all input gene annotation TSV file files within the specified directories
gene_annotation_files = []
gene_annotation_file_pattern = os.path.join(args.input, '01_GENOMAD', '*', '*_Genomad_Output', '*_summary', '*_virus_genes.tsv')
gene_annotation_files.extend(glob.glob(gene_annotation_file_pattern, recursive=True))
  
# Concatenate all DataFrames into a single DataFrame
dfs = []
for file in gene_annotation_files:
    df = pd.read_csv(file, sep='\t', usecols=['gene', 'start', 'end', 'length', 'strand', 'gc_content', 'genetic_code', 'rbs_motif', 'marker', 'evalue', 
                                              'bitscore', 'virus_hallmark', 'annotation_amr', 'annotation_accessions',
                                              'annotation_description'])
    dfs.append(df)

concatenated_gene_annotation_files = pd.concat(dfs, ignore_index=True)

# Define a custom sorting key function
def custom_sort_key(gene):
    # Split the viral_gene_id into parts using "_"
    parts = gene.split('_')
    Contig_name = '_'.join(parts[:-1])  # Join all parts except the last one to get Contig_name
    gene_number = int(parts[-1]) if len(parts) > 1 else 0  # Extract the last part as Gene_number

    return Contig_name, gene_number

# Sort rows by Contig_name and Gene_number using the custom sorting key
concatenated_gene_annotation_files[['Contig_name', 'Gene_number']] = concatenated_gene_annotation_files['gene'].apply(custom_sort_key).apply(pd.Series)
concatenated_gene_annotation_files.insert(1, 'Contig_name', concatenated_gene_annotation_files.pop('Contig_name'))
concatenated_gene_annotation_files.insert(2, 'Gene_number', concatenated_gene_annotation_files.pop('Gene_number'))

merged_concatenated_gene_annotation_files = pd.merge(quality_summary_concatenated_df[['virus_id']], concatenated_gene_annotation_files, left_on='virus_id',right_on='Contig_name', how='left')

merged_concatenated_gene_annotation_files = merged_concatenated_gene_annotation_files.sort_values(by=['Contig_name', 'Gene_number'], ascending=[True, True])
columns_to_fill_unknown = ['annotation_amr', 'annotation_accessions', 'annotation_description']
merged_concatenated_gene_annotation_files[columns_to_fill_unknown] = merged_concatenated_gene_annotation_files[columns_to_fill_unknown].fillna('Unknown')
merged_concatenated_gene_annotation_files.fillna('NA', inplace=True)
merged_concatenated_gene_annotation_files.drop(columns=['virus_id'], inplace=True)
merged_concatenated_gene_annotation_files.rename(columns={'gene': 'Viral_gene_ID'}, inplace=True)
merged_concatenated_gene_annotation_files['virus_hallmark'] = merged_concatenated_gene_annotation_files['virus_hallmark'].replace({0: 'NA', 1: 'Virus_hallmark'})
desired_column_order = ['Viral_gene_ID', 'Contig_name', 'Gene_number', 'start', 'end', 'length', 'strand', 'annotation_description', 'annotation_accessions', 'bitscore', 'evalue', 'gc_content', 'genetic_code', 'rbs_motif',
    'marker', 'virus_hallmark', 'annotation_amr']
merged_concatenated_gene_annotation_files = merged_concatenated_gene_annotation_files[desired_column_order]
new_column_names = {'gc_content': 'GENOMAD_gc_content', 'genetic_code': 'GENOMAD_genetic_code', 'rbs_motif': 'GENOMAD_rbs_motif', 'marker': 'GENOMAD_marker', 'evalue': 'GENOMAD_evalue',
    'bitscore': 'GENOMAD_Score',  'virus_hallmark': 'GENOMAD_virus_hallmark', 'annotation_amr': 'GENOMAD_Annotation_amr', 'annotation_accessions': 'GENOMAD_Annotation_accessions',
    'annotation_description': 'GENOMAD_Annotation'}
merged_concatenated_gene_annotation_files.rename(columns=new_column_names, inplace=True)
merged_concatenated_gene_annotation_files_path = os.path.join(args.input, '06_FUNCTIONAL_ANNOTATION', "All_Sample_Genomad_CheckV_Virus_Provirus_Gene_Annotation_GENOMAD.tsv")
merged_concatenated_gene_annotation_files.to_csv(merged_concatenated_gene_annotation_files_path, sep ='\t')

representative_merged_concatenated_gene_annotation_files = pd.merge(merged_quality_summary_concatenated_df[['Representative_Sequence']], merged_concatenated_gene_annotation_files, left_on='Representative_Sequence',right_on='Contig_name', how='left')
representative_merged_concatenated_gene_annotation_files.drop(columns=['Representative_Sequence'], inplace=True)
representative_merged_concatenated_gene_annotation_files_path = os.path.join(args.input, '06_FUNCTIONAL_ANNOTATION', "All_Sample_Genomad_CheckV_Virus_Provirus_Representative_Sequences_Gene_Annotation_GENOMAD.tsv")
representative_merged_concatenated_gene_annotation_files.to_csv(representative_merged_concatenated_gene_annotation_files_path, sep ='\t')

message1 = f"\n\033[1mModule 03 finished: virus genome clustering based on pairwise {args.min_ani}% ANI done and summary report generated!\033[0m"
message2 = f"Output files (clustering TXT files and representative virus sequence FASTA file) have been saved in the corresponding {args.input}/03_CLUSTERING directory."
message3 = f"summary_report.txt saved in {args.input}03_CLUSTERING."
message4 = f"Reference database from representative viral sequence FASTA file has been generated in {args.input}/04_READ_MAPPING.\n"
message4 = f"Representative viral sequence FASTA file and reference will be used as inputs for Module 04 (read mapping)."
message5 = f"Viral protein FASTA files has been generated in {args.input}/06_FUNCTIONAL_ANNOTATION directory and will be used as inputs for Module 06 (functional annotation)."
message6 = "\n\033[1mYou can now proceed to the next step of the MVP script: Module 04!\033[0m\n"
line_of_stars = '*' * len(message2)
print()
print(line_of_stars)
print(message1)
print(message2)
print(message3)
print(message4)
print(message5)
print(message6)
print(line_of_stars)
print()

print("Please don't forget to cite the following software used by this module:")
print("- Nayfach, S., Camargo, A.P., Schulz, F. et al. CheckV assesses the quality and completeness of metagenome-assembled viral genomes. Nat Biotechnol 39, 578–585 (2021). https://doi.org/10.1038/s41587-020-00774-7\n")
print("- Langmead B, Salzberg S. Fast gapped-read alignment with Bowtie 2. Nature Methods. 2012, 9:357-359\n")
print("- Li H. New strategies to improve minimap2 alignment accuracy. Bioinformatics. 2021, 37(23):4572–4574. https://academic.oup.com/bioinformatics/article/37/23/4572/6384570?login=true\n")