import os
import argparse
import subprocess
import pandas as pd
import shutil
import csv

# Parse command line arguments
parser = argparse.ArgumentParser(description='Module 03: Viral sequence clustering and reference preparation for the read mapping step.')
parser.add_argument('-i', '--input', help='Path to your working directory.', required=True)
parser.add_argument('-m', '--metadata', help='Input metadata containing sample names, nucleotide file paths, and read files.', required=True)
parser.add_argument('--fasta-files', type=str, default='representative', choices=['representative', 'all'], help='Sequence and protein FASTA files (representative or all sequences) to use for functional annotation. Default = representative')
parser.add_argument('--delete-files', action='store_true', help='flag to delete unwanted files')
parser.add_argument('--PHROGS-evalue', dest='phrogs_evalue', metavar='PHROGS_EVALUE', type=int, default=0.01, help='Significance e-value of match between target sequences and query (default = 0.01)')
parser.add_argument('--PHROGS-score', dest='phrogs_score', metavar='PHROGS_SCORE', type=int, default=50, help='Significant score of match between target sequences and query (default = 60)')
parser.add_argument('--PFAM-evalue', dest='pfam_evalue', metavar='PFAM_EVALUE', type=int, default=0.01, help='Significance e-value of match between target sequences and query (default = 0.01)')
parser.add_argument('--PFAM-score', dest='pfam_score', metavar='PFAM_SCORE', type=int, default=50, help='Significant score of match between target sequences and query (default = 60)')
parser.add_argument('--ADS-evalue', dest='ads_evalue', metavar='ADS_EVALUE', type=int, default=0.01, help='Significance e-value of match between target sequences and query (default = 0.01)')
parser.add_argument('--ADS-score', dest='ads_score', metavar='ADS_SCORE', type=int, default=60, help='Significant score of match between target sequences and query (default = 60)')
parser.add_argument('--ADS-seqid', dest='ads_seqid', metavar='ADS_SEQID', type=int, default=30, help='Significant score of match between target sequences and query (default = 60)')
parser.add_argument('--RdRP', action='store_true', help='Include this flag to create the 07_RDRP_PHYLOGENY folder and search RdRP profiles.')
parser.add_argument('--RdRP-evalue', dest='rdrp_evalue', metavar='RDRP_EVALUE', type=int, default=0.001, help='Significance e-value of match between target sequences and query (default = 0.01)')
parser.add_argument('--RdRP-score', dest='rdrp_score', metavar='RDRP_SCORE', type=int, default=50, help='Significant score of match between target sequences and query (default = 50)')
parser.add_argument('--DRAM', action='store_true', help='Include this flag to create a file to be process through DRAM-v.')
parser.add_argument('--force', action='store_true', help='Force execution of all steps, even if final_annotation_output_file exists.')
parser.add_argument('--threads', type=int, default=1, help='Number of threads to use (default = 1)')

# parse the arguments
args = parser.parse_args()

# Construct the path to the phrogs file
script_directory = os.path.dirname(os.path.abspath(__file__))
# Get the directory of the mvp directory
mvp_directory = os.path.dirname(script_directory)
# Load the PHROGS database files
phrogs_db = os.path.join(mvp_directory, 'data', 'PhrogDB_v14', 'phrogs_prof')
phrogs_index = os.path.join(mvp_directory, 'data', 'PhrogDB_v14', 'PHROGS_index_filter.csv')

# Load the PFAM database files
pfam_db = os.path.join(mvp_directory, 'data', 'Pfam_A_DB', 'pfam')
pfam_index = os.path.join(mvp_directory, 'data', 'Pfam_A_DB', 'PFAM_Index.tsv')

# Load the ADS database files
ADS_db = os.path.abspath(os.path.join(mvp_directory, 'data', 'ADS_DB', 'ADS_db'))
ADS_index = os.path.join(mvp_directory, 'data', 'ADS_DB', 'ADS_Index.txt')

if args.fasta_files == 'all':
    proteins_file = os.path.join(args.input, '06_FUNCTIONAL_ANNOTATION', 'All_Sample_Genomad_CheckV_Virus_Provirus_Sequences_proteins.faa')
    sequence_file = os.path.join(args.input, '03_CLUSTERING', 'All_Sample_Genomad_CheckV_Virus_Sequences.fna')
    genomad_annotation = os.path.join(args.input, '06_FUNCTIONAL_ANNOTATION', 'All_Sample_Genomad_CheckV_Virus_Provirus_Gene_Annotation_GENOMAD.tsv')
else:
    proteins_file = os.path.join(args.input, '06_FUNCTIONAL_ANNOTATION', 'All_Sample_Genomad_CheckV_Virus_Provirus_Representative_Sequences_proteins.faa')
    sequence_file = os.path.join(args.input, '03_CLUSTERING', 'All_Sample_Genomad_CheckV_Virus_Representative_Sequences.fna')
    genomad_annotation = os.path.join(args.input, '06_FUNCTIONAL_ANNOTATION', 'All_Sample_Genomad_CheckV_Virus_Provirus_Representative_Sequences_Gene_Annotation_GENOMAD.tsv')

# Check if the output proteins file exists
if not os.path.exists(proteins_file):
    # The file doesn't exist, so run Prodigal to predict proteins
    print("\nRunning Prodigal to predict proteins because protein file doesn't exist...\n")
    subprocess.run(['prodigal', '-i', sequence_file, '-a', proteins_file])
else:
    print("\nSkipping Prodigal to predict proteins because protein file already't exist...\n")

if args.RdRP:
    final_annotation_output_file = os.path.join(args.input, '06_FUNCTIONAL_ANNOTATION', os.path.splitext(os.path.basename(genomad_annotation))[0] + '_PHROGS_PFAM_ADS_RDRP_Filtered.tsv')
else:
    final_annotation_output_file = os.path.join(args.input, '06_FUNCTIONAL_ANNOTATION', os.path.splitext(os.path.basename(genomad_annotation))[0] + '_PHROGS_PFAM_ADS.tsv')

phrogs_output_file = os.path.join(args.input, '06_FUNCTIONAL_ANNOTATION', os.path.basename(genomad_annotation).replace('_GENOMAD.tsv', '_PHROGS.tsv'))
pfam_output_file = os.path.join(args.input, '06_FUNCTIONAL_ANNOTATION', os.path.basename(genomad_annotation).replace('_GENOMAD.tsv', '_PFAM.tsv'))
ADS_output_file = os.path.join(args.input, '06_FUNCTIONAL_ANNOTATION', os.path.basename(genomad_annotation).replace('_GENOMAD.tsv', '_ADS.tsv'))

if not args.force and os.path.exists(final_annotation_output_file):
    print(f"\n'{final_annotation_output_file}' exists, and --force argument is not provided. Skipping functional annotation steps...\n")
else:
    if os.path.exists(phrogs_output_file) and os.path.exists(pfam_output_file) and os.path.exists(phrogs_output_file):
        print(f"\n'PHROGS, PFAM, ADS annotation output files exist. Skipping functional annotation steps but a final merged annotation output file {final_annotation_output_file} will be created...\n")
    else:
        # create mmseqs DB
        temp_directory = os.path.join(args.input, '06_FUNCTIONAL_ANNOTATION', 'tmp')
        target_sequences = os.path.join(args.input, '06_FUNCTIONAL_ANNOTATION', os.path.splitext(os.path.basename(proteins_file))[0] + '_mmseqs2_target_sequences')
        if os.path.exists(target_sequences):
            print(f'\nSkipping creating phrogs database for {proteins_file}, {target_sequences} files already exist\n')
        else:
            print(f'\nCreating {target_sequences}...\n')
            subprocess.run(['mmseqs', 'createdb', proteins_file, target_sequences])

        # run mmseqs search against PHROGS database
        phrogs = os.path.join(args.input, '06_FUNCTIONAL_ANNOTATION', os.path.splitext(os.path.basename(proteins_file))[0] + '_mmseqs2_PHROGS')
        if os.path.exists(phrogs_output_file):
            print(f'\nSkipping PHROGS annotation, {phrogs_output_file} file already exists\n')
        else:
            if os.path.exists(phrogs):
                print(f'\nSkipping mmseqs search for {target_sequences}, {phrogs} file already exists\n')
            else:
                print(f'\nCreating {phrogs} files...\n')
                subprocess.run(['mmseqs', 'search', phrogs_db, target_sequences, phrogs, temp_directory, '-s', '7', '--threads', str(args.threads)])

            # create TSV file
            # Define a custom sorting key function
            def custom_sort_key(viral_gene_id):
                # Split the viral_gene_id into parts using "_"
                parts = viral_gene_id.split('_')
                Contig_name = '_'.join(parts[:-1])  # Join all parts except the last one to get Contig_name
                gene_number = int(parts[-1]) if len(parts) > 1 else 0  # Extract the last part as Gene_number
                return Contig_name, gene_number

            subprocess.run(['mmseqs', 'createtsv', phrogs_db, target_sequences, phrogs, phrogs_output_file])

            # Sort rows by Contig_name and Gene_number using the custom sorting key
            phrogs_output_file_df = pd.read_csv(phrogs_output_file, sep='\t', encoding_errors='ignore', names=['#phrog', 'Viral_gene_ID', 'PHROGS_Score', 'PHROGS_seqIdentity', 'PHROGS_evalue', 'qStart', 'qEnd', 'qLen', 'tStart', 'tEnd', 'tLen'])
            phrogs_output_file_df['#phrog'] = phrogs_output_file_df['#phrog'].str.replace('.fma', '')
            phrogs_output_file_df[['Contig_name', 'Gene_number']] = phrogs_output_file_df['Viral_gene_ID'].apply(custom_sort_key).apply(pd.Series)
            phrogs_output_file_df.insert(2, 'Contig_name', phrogs_output_file_df.pop('Contig_name'))
            phrogs_output_file_df.insert(3, 'Gene_number', phrogs_output_file_df.pop('Gene_number'))
            phrogs_output_file_df = phrogs_output_file_df.sort_values(by=['Contig_name', 'Gene_number'], ascending=[True, True])

            # Merge phrogs_output_csv_file and phrogs_index based on the '#phrog' column
            phrogs_index_df = pd.read_csv(phrogs_index)
            annotation_phrogs_output_file_df = pd.merge(phrogs_output_file_df, phrogs_index_df, on='#phrog', how='left').fillna('Unknown')
            annotation_phrogs_output_file_df['PHROGS_Category'] = annotation_phrogs_output_file_df['PHROGS_Category'].str.replace(' function', '')
            annotation_phrogs_output_file_df = annotation_phrogs_output_file_df.drop(columns=annotation_phrogs_output_file_df.columns[0])
            annotation_phrogs_output_file_df.to_csv(phrogs_output_file, sep ='\t')

        # run mmseqs search against PFAM database
        pfam = os.path.join(args.input, '06_FUNCTIONAL_ANNOTATION', os.path.splitext(os.path.basename(proteins_file))[0] + '_mmseqs2_PFAM')
        if os.path.exists(pfam_output_file):
            print(f'\nSkipping PFAM annotation, {pfam_output_file} file already exists\n')
        else:
            if os.path.exists(pfam):
                print(f'\nSkipping mmseqs search for {target_sequences}, {pfam} file already exists\n')
            else:
                print(f'\nCreating {pfam} files...\n')
                subprocess.run(['mmseqs', 'search', pfam_db, target_sequences, pfam, temp_directory, '-s', '7', '--threads', str(args.threads)])

            subprocess.run(['mmseqs', 'createtsv', pfam_db, target_sequences, pfam, pfam_output_file])

            # Read pfam annotation table to merge it with pfam index
            pfam_output_file_df = pd.read_csv(pfam_output_file, sep='\t', encoding_errors='ignore', names=['PFAM_Accession_Number', 'Viral_gene_ID', 'PFAM_Score', 'PFAM_seqIdentity', 'PFAM_evalue', 'qStart', 'qEnd', 'qLen', 'tStart', 'tEnd', 'tLen'])
            pfam_output_file_df['PFAM_Accession_Number'] = pfam_output_file_df['PFAM_Accession_Number'].str.split('.').str[0]
            pfam_index_df = pd.read_csv(pfam_index, sep='\t', encoding_errors='ignore')
            annotation_pfam_output_file_df = pd.merge(pfam_output_file_df, pfam_index_df, left_on = 'PFAM_Accession_Number', right_on = '#pfam', how='left').fillna('Unknown')
            annotation_pfam_output_file_df.to_csv(pfam_output_file, sep ='\t')

            # ADS prediction
            print("\nRunning BLASTP to search Anti-Defense Systems against a protein sequence database...\n")
            subprocess.run(['blastp', '-query', proteins_file, '-db', ADS_db, '-out', ADS_output_file, '-outfmt', '6 std qlen slen nident positive'])

            ADS_df = pd.read_csv(ADS_output_file, sep = '\t', encoding_errors='ignore', names=['Viral_gene_ID', 'subject_id', 'ADS_seqIdentity', 'ADS_alignment_length', 'ADS_mismatches',
                                                                                                'ADS_gap_opens', 'ADS_q_Start', 'ADS_q_End', "ADS_s_start", "ADS_s_end", "ADS_evalue", 
                                                                                                "ADS_Score", "ADS_std", "ADS_qlen", "ADS_slen", "ADS_nident"])
            ADS_Index_df = pd.read_csv(ADS_index, sep = '\t', encoding_errors='ignore')
            ADS_Index_df = pd.merge(ADS_df, ADS_Index_df[['subject_id', 'ADS_Annotation', 'ADS_acr_families']], how = 'left', on = 'subject_id')
            ADS_Index_df.to_csv(ADS_output_file, sep='\t', index=False)

    # sort, filter and merge all dataframes
    annotation_phrogs_output_file_df = pd.read_csv(phrogs_output_file, sep='\t', encoding_errors='ignore', usecols=['Viral_gene_ID', 'PHROGS_Score', 'PHROGS_seqIdentity', 'PHROGS_evalue', 'PHROGS_Annotation',
                                                                                                        'PHROGS_Category', 'PHROGS_RefSeq_Annotation', 'PHROGS_Pfam_Annotation',
                                                                                                        'PHROGS_GO_Annotation', 'PHROGS_KO_Annotation'])
    filter_annotated_phrogs_output_file_df = annotation_phrogs_output_file_df.loc[(annotation_phrogs_output_file_df['PHROGS_Score'] >= args.phrogs_score) & (annotation_phrogs_output_file_df['PHROGS_evalue'] <= args.phrogs_evalue)]
    filter_annotated_phrogs_output_file_df = annotation_phrogs_output_file_df.sort_values('PHROGS_Score', ascending=False).drop_duplicates('Viral_gene_ID').sort_index()

    genomad_annotation_df = pd.read_csv(genomad_annotation, sep='\t')
    all_genes_filter_annotated_phrogs_output_file_df = pd.merge(genomad_annotation_df, filter_annotated_phrogs_output_file_df, on='Viral_gene_ID', how='left').fillna('NA')
    
    annotation_pfam_output_file_df = pd.read_csv(pfam_output_file, sep='\t', encoding_errors='ignore', usecols=['Viral_gene_ID', 'PFAM_Accession_Number', 'PFAM_Annotation', 'PFAM_Annotation_Short', 'PFAM_Category', 'PFAM_seqIdentity', "PFAM_Score", "PFAM_evalue"])
    PFAM_Index_Filter_df = annotation_pfam_output_file_df.loc[(annotation_pfam_output_file_df['PFAM_Score'] >= args.pfam_score) & (annotation_pfam_output_file_df['PFAM_evalue'] <= args.pfam_evalue)]
    PFAM_Index_Filter_df = PFAM_Index_Filter_df.sort_values('PFAM_Score', ascending=False).drop_duplicates('Viral_gene_ID').sort_index()
    PFAM_Index_Filter_annotated_phrogs_output_file = os.path.join(args.input, '06_FUNCTIONAL_ANNOTATION', os.path.splitext(os.path.basename(genomad_annotation))[0] + '_PHROGS_PFAM_Filtered.tsv')
    PFAM_GENOMAD_PHROGS_df = pd.merge(all_genes_filter_annotated_phrogs_output_file_df, PFAM_Index_Filter_df, on='Viral_gene_ID', how='left').fillna('NA')

    ADS_Index_df = pd.read_csv(ADS_output_file, sep = '\t', encoding_errors='ignore', usecols=['Viral_gene_ID', 'ADS_Annotation', 'ADS_acr_families', 'ADS_seqIdentity', "ADS_evalue", "ADS_Score"])
    ADS_Index_Filter_df = ADS_Index_df.loc[(ADS_Index_df['ADS_Score'] >= args.ads_score) & (ADS_Index_df['PFAM_seqIdentity'] <= args.ads_seqid) & (ADS_Index_df['ADS_evalue'] <= args.ads_evalue)]
    ADS_Index_Filter_df = ADS_Index_Filter_df.sort_values('ADS_Score', ascending=False).drop_duplicates('Viral_gene_ID').sort_index()
    ADS_GENOMAD_PHROGS_PFAM_df = pd.merge(PFAM_GENOMAD_PHROGS_df, ADS_Index_Filter_df, on='Viral_gene_ID', how='left').fillna('NA')
    desired_column_order = ['Viral_gene_ID', 'Contig_name', 'Gene_number', 'start', 'end', 'length', 'strand', 'GENOMAD_Annotation', 'PHROGS_Annotation', 'PHROGS_Category', 'PFAM_Annotation',	'PFAM_Category',
                            'ADS_Annotation', 'ADS_acr_families', 'GENOMAD_Annotation_accessions', 'GENOMAD_Score', 'GENOMAD_evalue', 'GENOMAD_gc_content', 'GENOMAD_genetic_code', 'GENOMAD_rbs_motif', 'GENOMAD_marker',
                            'GENOMAD_virus_hallmark', 'GENOMAD_Annotation_amr', 'PHROGS_Score', 'PHROGS_evalue', 'PHROGS_seqIdentity', 'PHROGS_RefSeq_Annotation', 'PHROGS_Pfam_Annotation',
                            'PHROGS_GO_Annotation', 'PHROGS_KO_Annotation', 'PFAM_Accession_Number', 'PFAM_Annotation_Short', 'PFAM_seqIdentity', 'PFAM_Score', 'PFAM_evalue', 'ADS_seqIdentity', 'ADS_Score', 'ADS_evalue']
    ADS_GENOMAD_PHROGS_PFAM_df = ADS_GENOMAD_PHROGS_PFAM_df[desired_column_order]

    # Create the "07_RDRP_PHYLOGENY" directory if --RdRP flag is provided
    if args.RdRP:
        print("\nRunning HMMSEARCH to search RdRP profiles...\n")
        rdrp_phylogeny_dir = os.path.join(args.input, '07_RDRP_PHYLOGENY')
        os.makedirs(rdrp_phylogeny_dir, exist_ok=True)

        # Load the RdRP profile HMM file
        RdRP_profile = os.path.abspath(os.path.join(mvp_directory, 'data', 'All_RdRP_profiles_Wolf_set.22.hmm'))

        # Perform hmmsearch
        subprocess.run(['hmmsearch', '-o', os.path.join(rdrp_phylogeny_dir, '07A_RdRP_Profile_Output.txt'), '--tblout', os.path.join(rdrp_phylogeny_dir, '07A_RdRP_Profile_Tab.txt'), RdRP_profile, proteins_file])
        
        RdRP_Profile_Output = os.path.join(rdrp_phylogeny_dir, '07A_RdRP_Profile_Tab.txt')
        Formatted_RdRP_Profile_Output = os.path.join(rdrp_phylogeny_dir, '07B_Formatted_RdRP_Profile_Tab.tsv')
        Filtered_Formatted_RdRP_Profile_Output = os.path.join(rdrp_phylogeny_dir, '07C_Filtered_Formatted_RdRP_Profile_Tab.tsv')

        with open(RdRP_Profile_Output, 'r') as infile:
            lines = infile.readlines()

        # Remove the first three rows and the last rows containing '#'
        lines = [line for line in lines[3:] if not line.startswith('#')]
        data = [line.strip().split() for line in lines]

        # Filter the data to keep only the desired columns if they exist
        filtered_data = []
        for row in data:
            if len(row) >= 7:
                filtered_data.append([row[0], row[2], row[4], row[5]])

        # Create a DataFrame from the filtered data
        Formatted_RdRP_Profile_Output_df = pd.DataFrame(filtered_data, columns=["Viral_gene_ID", "RDRP_Annotation", "RDRP_evalue", "RDRP_Score"])
        Formatted_RdRP_Profile_Output_df.to_csv(Formatted_RdRP_Profile_Output, sep='\t', index=False)

        # Convert the 'score' column to numeric
        Formatted_RdRP_Profile_Output_df['RDRP_Score'] = pd.to_numeric(Formatted_RdRP_Profile_Output_df['RDRP_Score'], errors='coerce')
        Formatted_RdRP_Profile_Output_df['RDRP_evalue'] = pd.to_numeric(Formatted_RdRP_Profile_Output_df['RDRP_evalue'], errors='coerce')

        Filtered_Formatted_RdRP_Profile_Output_df = Formatted_RdRP_Profile_Output_df.sort_values('RDRP_Score').drop_duplicates('Viral_gene_ID', keep = 'last')
        Filtered_Formatted_RdRP_Profile_Output_df = Filtered_Formatted_RdRP_Profile_Output_df[Filtered_Formatted_RdRP_Profile_Output_df['RDRP_Score'] >= args.rdrp_score]
        Filtered_Formatted_RdRP_Profile_Output_df = Filtered_Formatted_RdRP_Profile_Output_df[Filtered_Formatted_RdRP_Profile_Output_df['RDRP_evalue'] <= args.rdrp_evalue]
        Filtered_Formatted_RdRP_Profile_Output_df.to_csv(Filtered_Formatted_RdRP_Profile_Output, sep='\t', index=False)

        RDRP_GENOMAD_PHROGS_PFAM_ADS_df = pd.merge(ADS_GENOMAD_PHROGS_PFAM_df, Filtered_Formatted_RdRP_Profile_Output_df, on='Viral_gene_ID', how='left').fillna('NA')
        RDRP_GENOMAD_PHROGS_PFAM_ADS_df.insert(12, 'RDRP_Annotation', RDRP_GENOMAD_PHROGS_PFAM_ADS_df.pop('RDRP_Annotation'))
        RDRP_GENOMAD_PHROGS_PFAM_ADS_df.to_csv(final_annotation_output_file, sep='\t', index=False)
    else:
        ADS_GENOMAD_PHROGS_PFAM_df.to_csv(final_annotation_output_file, sep='\t', index=False)

        # Calculate summary statistics
        num_genes = len(final_annotation_output_file)
        num_hit_genes = len(phrogs_output_file)

        # Count occurrences of each realm in the second level of the taxonomy column
        category_levels = phrogs_output_file['PHROGS_Category']
        category_counts = category_levels.value_counts()
        # Calculate the percentage of each realm
        taxa_percentages = (category_counts / num_genes) * 100
        # Prepare the summary report content
        category_summary = "\n".join([f"{category}: {count} ({percentage:.1f}%)" for category, count, percentage in zip(category_counts.index, category_counts.values, taxa_percentages.values)])

        # Define the path to the summary report created in Module 01
        summary_report_path_module_05 = os.path.join(args.input, '05_VOTU_TABLES', 'MVP_05_Summary_Report.txt')

        # Open the summary report created in Module 01 and read its content
        with open(summary_report_path_module_05, 'r') as module_05_summary_report:
            module_05_summary_content = module_05_summary_report.read()

        # Define the additional lines you want to add to the summary report
        module_06_lines_to_add = """
        ****************************************************************************
        ******************               MODULE 06                ******************
        ****************************************************************************
        """

        # Create a dictionary to hold argument descriptions and their default values
        argument_defaults = {
            '--input': args.input,
            '--metadata': args.metadata,
            '--fasta-files': args.fasta_files}

        # Write a summary line with script arguments and their default values
        summary_line = "06_do_functional_annotation.py"
        for arg, default in argument_defaults.items():
            if default is not None:
                summary_line += f" {arg} {default}"

        # Combine the content of Module 01 and Module 02 lines
        complete_summary_content = module_05_summary_content + module_06_lines_to_add + summary_line

        complete_summary_content +=f"""
        \nSummary Report of the functional annotation after filtration
        --------------------------
        Number of genes: {num_genes}
        Number of hits against PHROGS db: {num_hit_genes}

        Gene category summary:
        {category_summary}
        """

        summary_report_path_module_06 = os.path.join(args.input, '06_FUNCTIONAL_ANNOTATION', 'MVP_06_Summary_Report.txt')

        # Write the combined content to the new summary report for Module 02
        with open(summary_report_path_module_06, 'w') as module_06_summary_report:
            module_06_summary_report.write(complete_summary_content)

# Create a file to be process through DRAM-v if --DRAM flag is provided
DRAM_input_file = os.path.join(args.input, '06_FUNCTIONAL_ANNOTATION', os.path.splitext(os.path.basename(genomad_annotation))[0] + '_DRAM_Input.tsv')
if args.DRAM and not os.path.exists(DRAM_input_file):
    def  main():
        print("\nCreating an input file for DRAM-v functional annotation...\n")
        annotation_df = pd.read_csv(final_annotation_output_file, sep = '\t', encoding_errors='ignore', usecols= ['Viral_gene_ID', 'start', 'end', 'length','strand', 'GENOMAD_marker',
                                                                                                                'GENOMAD_Score', 'GENOMAD_evalue', 'GENOMAD_virus_hallmark',
                                                                                                                'PFAM_Annotation', 'PFAM_Annotation_Short', 'PFAM_Accession_Number', 'PFAM_Score', 'PFAM_evalue'])
        print(annotation_df)
        # Add info about hallmark gene in the VirSorter format
        annotation_df['VS_hallmark'] = annotation_df[['GENOMAD_marker','GENOMAD_virus_hallmark']].apply(lambda x: guess_vs_hallmark(x), axis=1)
        # Also add info about the original contig
        annotation_df['contig'] = annotation_df['Viral_gene_ID'].apply(lambda x: get_contig_name(x))
        # Prepare the output file
        with open(DRAM_input_file, "w") as output_handle:
            outwriter = csv.writer(output_handle, delimiter='|',quotechar='"', quoting=csv.QUOTE_MINIMAL, lineterminator="\n")
            # outwriter.writerow(['Spacer','Spacer_cluster','Spacer_Length','Spacer_Coverage','Repeat_id'])
            annotation_df.groupby('contig').apply(lambda x: write_output(x,outwriter,output_handle))
                    
    def guess_vs_hallmark(vec):
        if vec['GENOMAD_marker'] == "NA" or pd.isna(vec['GENOMAD_marker']):
            return "-"
        t = vec['GENOMAD_marker'].split(".")
        if t[2].startswith("V") or t[2].endswith("V"):
            ## We have a virus gene, is it hallmark ? (note - hallmark coded as 0 in VirSorter, 1 in geNomad)
            if vec['GENOMAD_virus_hallmark'] == 1:
                return "0"
            else:
                return "1"
        else:
            return "-"
        
    def get_contig_name(gene_name):
        x = gene_name.split("_")
        return "_".join(x[:-1])

    def write_output(df,outwriter,output_handle):
        ## First add a line for the contig itself
        contig = df['contig'].unique()[0]
        n_gene = len(df)
        output_handle.write(f">{contig}|{n_gene}|l\n") ## Note - we falsely claim all sequences are linear, it does not matter really for DRAM-V downstream
        # Now adding one line for each gene, sorted by start
        df.sort_values(['start','strand'], ascending=[True,True]).apply(lambda x: add_line(x,outwriter),axis=1)

    def add_line(vec,outwriter):
        ## Convert strand notation in VirSorter format (plus and minus)
        if vec['strand'] == -1:
            strand = "-"
        elif vec["strand"] == 1:
            strand = "+"
        ## Convert marker gene name if na
        if vec['VS_hallmark'] == "-":
            vec['GENOMAD_marker'] = "-"
            vec['GENOMAD_Score'] = "-"
            vec['GENOMAD_evalue'] = "-"
    
        if pd.isna(vec['PFAM_Annotation_Short']):
            PFAM_Annotation_Short = "-"
            PFAM_Score = "-"
            PFAM_evalue = "-"
        else:
            PFAM_Annotation_Short = vec['PFAM_Annotation_Short']
            PFAM_Score = vec['PFAM_Score']
            PFAM_evalue = vec['PFAM_evalue']

        outwriter.writerow([vec['Viral_gene_ID'],vec['start'],vec['end'],vec['length'],strand,vec['GENOMAD_marker'],vec['GENOMAD_Score'],vec['GENOMAD_evalue'],vec['VS_hallmark'],PFAM_Annotation_Short,PFAM_Score,PFAM_evalue])

    if __name__ == "__main__":
        output = main()

if args.delete_files:
    print("\nDeleting temporary files...\n")
    # Define the directory where you want to remove files
    directory_to_clean = os.path.join(args.input, '06_FUNCTIONAL_ANNOTATION')
    # Remove temporary files and folders
    for filename in os.listdir(directory_to_clean):
        if filename.endswith(('.faa', '.tsv')):
            continue  # Keep .faa and .tsv files
        file_path = os.path.join(directory_to_clean, filename)
        if os.path.isfile(file_path):
            os.remove(file_path)

    # Remove temporary files and folders
    shutil.rmtree(temp_directory)

message1 = f"\n\033[1mModule 06 finished: functional annotation done and summary report generated!\033[0m"
message2 = f"PHROGS annotation output file has been saved in the corresponding {args.input}/06_FUNCTIONAL_ANNOTATION directory."
message3 = f"summary_report.txt saved in {args.input}/06_FUNCTIONAL_ANNOTATION."
message4 = "\n\033[1mWe hope you enjoyed using MVP script, and you can now explore your data!\033[0m\n"
line_of_stars = '*' * len(message2)
print()
print(line_of_stars)
print(message1)
print(message2)
print(message3)
print(message4)
print(line_of_stars)
print()

print("Please don't forget to cite the following software used by this module:")
print("- Hyatt, D. et al. Prodigal: Prokaryotic Gene Recognition and Translation Initiation Site Identification. BMC Bioinformatics. 2010, 11(1). https://doi.org/10.1186/1471-2105-11-119.\n")
print("- Mirdita, M. et al. Fast and sensitive taxonomic assignment to metagenomic contigs. Bioinformatics. 2021, 37(18). https://doi.org/10.1093/bioinformatics/btab184\n")
print("- Potter, S. et al. HMMER Web Server: 2018 Update. Nucleic Acids Research. 2018, 46(1). https://doi.org/10.1093/nar/gky448.\n")