import argparse
import glob
import os
import sys
import pandas as pd


# Parse command line arguments
parser = argparse.ArgumentParser(description='Module 02: find geNomad and Check_V output summary files, merge and filter them.')
parser.add_argument('-i', '--input', help='Path to your working directory.', required=True)
parser.add_argument('-m', '--metadata', help='Input metadata containing sample names, nucleotide file paths, and read files.', required=True)
parser.add_argument('--sample-group', help='Specific sample number(s) to run the script on (can be a comma-separated list: 1,2,6 for example). By default, MVP processes all datasets listed in the metadata file one after the other.', type=str, default='')
parser.add_argument('--viral-min-genes', dest='viral_min_genes', metavar='VIRAL_MIN_GENES', type=int, default=1, help='the minimum number of viral genes required to consider a row as a virus prediction (default = 1)')
parser.add_argument('--host-viral-genes-ratio', dest='host_viral_genes_ratio', metavar='HOST_VIRAL_GENES_RATIO', type=float, default=1.0, help='the maximum ratio of host genes to viral genes required to consider a row as a virus prediction (default = 1)')

args = parser.parse_args()

# Read the metadata file
metadata_df = pd.read_csv(args.metadata, sep='\t')

# Construct the path to the ICTV_Taxonomy_List.tsv file
script_directory = os.path.dirname(os.path.abspath(__file__))
# Get the directory of the mvp directory
mvp_directory = os.path.dirname(script_directory)
# Load the ICTV Taxonomy List file
ictv_taxonomy_file = os.path.join(mvp_directory, 'data', 'ICTV_Taxonomy_List.tsv')
# Read the ICTV Taxonomy List file
ictv_taxonomy_df = pd.read_csv(ictv_taxonomy_file, sep='\t')

# Define a function to find closest matching taxonomy and return Genome type and Host type
def find_closest_match(taxonomy, ictv_df):
    taxonomy_levels = taxonomy.split(';')
    matching_rows = []

    for i in range(len(taxonomy_levels), 0, -1):
        partial_taxonomy = ';'.join(taxonomy_levels[:i])
        match_rows = ictv_df[ictv_df['Family'] == partial_taxonomy]
        if not match_rows.empty:
            matching_rows.extend(match_rows.iterrows())

    if matching_rows:
        return matching_rows[0][1][['Genome type', 'Host type']]
    else:
        return pd.Series({'Genome type': 'Unknown', 'Host type': 'Unknown'})

# Define a function to map taxonomy to genome type
def taxon_to_genome_type(taxonomy):
    genome_host_info = find_closest_match(taxonomy, ictv_taxonomy_df)
    return genome_host_info['Genome type']

# Define a function to map taxonomy to host type
def taxon_to_host_type(taxonomy):
    genome_host_info = find_closest_match(taxonomy, ictv_taxonomy_df)
    return genome_host_info['Host type']

# Initialize a counter for processed samples
processed_samples_count = 0

# Process rows based on specific sample numbers or all rows
specific_sample_numbers = []
if args.sample_group:
    specific_sample_numbers = [int(num) for num in args.sample_group.split(',') if num.strip()]

for row_index, (_, row) in enumerate(metadata_df.iterrows(), start=1):
    input_assembly_file = row['Assembly_Path']
    sample_number = row['Sample_number']
    sample_name = row['Sample']

    # Check if the current sample number is in the list of specific sample numbers
    if specific_sample_numbers and sample_number not in specific_sample_numbers:
        continue # Skip to the next iteration

    # Construct the path to the quality_summary.tsv file based on the sample name
    virus_checkv_file = os.path.join(args.input, '02_CHECK_V', str(sample_name), f'{sample_name}_Viruses_CheckV_Output/quality_summary.tsv')
    # Check if the CheckV files directory exists
    if not os.path.exists(virus_checkv_file):
        print(f"Error: Virus CheckV file '{virus_checkv_file}' does not exist.\n")
        sys.exit(1)
    # Construct paths to other corresponding files
    provirus_checkv_file = os.path.join(args.input, '02_CHECK_V', str(sample_name), f'{sample_name}_Proviruses_CheckV_Output/quality_summary.tsv')
    if not os.path.exists(provirus_checkv_file):
        print(f"Error: Provirus CheckV file '{provirus_checkv_file}' does not exist.\n")
        sys.exit(1)
    virus_genomad_file = glob.glob(os.path.join(args.input, '01_GENOMAD', str(sample_name), f'{sample_name}_Viruses_Genomad_Output/*/*_virus_summary.tsv'))[0]
    if not os.path.exists(virus_genomad_file):
        print(f"Error: Virus geNomad file '{virus_genomad_file}' does not exist.\n")
        sys.exit(1)
    provirus_genomad_file = glob.glob(os.path.join(args.input, '01_GENOMAD', str(sample_name), f'{sample_name}_Proviruses_Genomad_Output/*/proviruses_virus_summary.tsv'))[0]
    if not os.path.exists(provirus_genomad_file):
        print(f"Error: Provirus geNomad file '{provirus_genomad_file}' does not exist.\n")
        sys.exit(1)

    # Read Genomad and CheckV files into a dataframe
    df_virus_CHECKV = pd.read_csv(virus_checkv_file, sep='\t', usecols=['contig_id', 'contig_length', 'provirus', 'proviral_length', 'gene_count', 'viral_genes',
                                                                         'host_genes', 'checkv_quality', 'miuvig_quality', 'completeness', 'completeness_method', 'kmer_freq'])
    df_provirus_CHECKV = pd.read_csv(provirus_checkv_file, sep='\t', usecols=['contig_id', 'contig_length', 'provirus', 'proviral_length', 'gene_count', 'viral_genes',
                                                                         'host_genes', 'checkv_quality', 'miuvig_quality', 'completeness', 'completeness_method', 'kmer_freq'])
    df_virus_GENOMAD = pd.read_csv(virus_genomad_file, sep='\t', usecols=['seq_name', 'topology', 'genetic_code', 'virus_score', 'n_hallmarks', 'marker_enrichment', 'taxonomy', 'coordinates'])
    df_provirus_GENOMAD = pd.read_csv(provirus_genomad_file, sep='\t', usecols=['seq_name', 'topology', 'genetic_code', 'virus_score', 'n_hallmarks', 'marker_enrichment', 'taxonomy', 'coordinates'])

    # Filter out provirus lines in df_virus_CHECKV
    df_virus_CHECKV = df_virus_CHECKV[df_virus_CHECKV['provirus'] != 'Yes']

    # Merge the filtered virus CheckV and proviruses CheckV DataFrames
    checkv_merged_df = pd.concat([df_virus_CHECKV, df_provirus_CHECKV], ignore_index=True)

    # Filter the merged dataframe to keep only rows predicted as 'virus'
    filter_checkv_merged_df = checkv_merged_df.loc[(checkv_merged_df['viral_genes'] >= args.viral_min_genes)]
    filter_checkv_merged_df = filter_checkv_merged_df.loc[filter_checkv_merged_df['host_genes']/filter_checkv_merged_df['viral_genes'] <= args.host_viral_genes_ratio]

    # Merge the  virus Genomad and proviruses Genomad DataFrames
    genomad_merged_df = pd.concat([df_virus_GENOMAD, df_provirus_GENOMAD], ignore_index=True)
    # Merge the  merged CheckV and the merged Genomad DataFrames
    checkv_genomad_merged_df = pd.merge(filter_checkv_merged_df, genomad_merged_df, left_on='contig_id',right_on='seq_name', how='left')
    checkv_genomad_merged_df = checkv_genomad_merged_df.rename(columns={'contig_id': 'virus_id', 'contig_length': 'virus_length'}).drop('proviral_length', axis=1)
    # Cast the 'topology' column to string type
    checkv_genomad_merged_df['topology'] = checkv_genomad_merged_df['topology'].astype(str)
    checkv_genomad_merged_df['virus_id'] = checkv_genomad_merged_df['virus_id'].astype(str)

    # Add part of the virus_id name to the coordinates column for rows where 'provirus' is 'Yes'
    checkv_genomad_merged_df.loc[checkv_genomad_merged_df['provirus'] == 'Yes', 'coordinates'] = checkv_genomad_merged_df.loc[checkv_genomad_merged_df['provirus'] == 'Yes', 'virus_id'].str.split('|').str[-1].str.split('/').str[0]

    # Update the 'provirus' column for rows where 'topology' contains 'Provirus'
    checkv_genomad_merged_df.loc[checkv_genomad_merged_df['topology'].str.contains('Provirus'), 'provirus'] = 'Yes'
    # Change 'No terminal repeats' to 'Provirus' for rows where 'Yes' is present in the 'provirus' column
    checkv_genomad_merged_df.loc[checkv_genomad_merged_df['provirus'] == 'Yes', 'topology'] = checkv_genomad_merged_df.loc[checkv_genomad_merged_df['provirus'] == 'Yes', 'topology'].replace('No terminal repeats', 'Provirus')
    checkv_genomad_merged_df = checkv_genomad_merged_df.fillna('NA')

    # Apply the functions to each row in checkv_genomad_merged_df
    checkv_genomad_merged_df['Genome type'] = checkv_genomad_merged_df['taxonomy'].apply(taxon_to_genome_type)
    checkv_genomad_merged_df['Host type'] = checkv_genomad_merged_df['taxonomy'].apply(taxon_to_host_type)

    # Exclude specific columns before saving the DataFrame
    columns_to_exclude = ['seq_name', 'topology']  # Specify the columns to exclude
    # Drop the specified columns
    checkv_genomad_merged_df = checkv_genomad_merged_df.drop(columns=columns_to_exclude)

    # Save the merged Checkv and Genomad file
    checkv_genomad_merged_output_file = os.path.join(args.input, '02_CHECK_V', str(sample_name), f'{sample_name}_Genomad_CheckV_Virus_Proviruses_Quality_Summary.tsv')
    virus_ids_to_keep = set(checkv_genomad_merged_df['virus_id'].tolist())
    checkv_genomad_merged_df.to_csv(checkv_genomad_merged_output_file, sep='\t', index=False)
    print(f"\nMerged Checkv and Genomad quality summary file saved: {checkv_genomad_merged_output_file}")

    # Find the directory containing the viruses and proviruses fasta files for this sample
    virus_fasta_file = glob.glob(os.path.join(args.input, '02_CHECK_V', str(sample_name), f'{sample_name}_Viruses_CheckV_Output/viruses.fna'))[0]
    provirus_fasta_file = glob.glob(os.path.join(args.input, '02_CHECK_V', str(sample_name), f'{sample_name}_Proviruses_CheckV_Output/viruses.fna'))[0]

    virus_fasta_lines = []
    provirus_fasta_lines = []
    if os.path.isfile(virus_fasta_file):
        with open(virus_fasta_file, 'r') as f:
            virus_fasta_lines = f.readlines()
    if os.path.isfile(provirus_fasta_file):
        with open(provirus_fasta_file, 'r') as f:
            provirus_fasta_lines = f.readlines()

    # Concatenate the virus and provirus fasta sequences
    concatenated_fasta_lines = virus_fasta_lines + provirus_fasta_lines

    # Filter the concatenated file
    filtered_concatenated_fasta_file = os.path.join(args.input, '02_CHECK_V', str(sample_name), f'{sample_name}_virus_provirus.fna')
    with open(filtered_concatenated_fasta_file, 'w') as f:
        for line in concatenated_fasta_lines:
            if line.startswith('>'):
                contig_id = line.strip()[1:]
                if contig_id in virus_ids_to_keep:
                    write_line = True
                    f.write(line)
                else:
                    write_line = False
            else:
                if write_line:
                    f.write(line)
    print(f"Concatenated virus and provirus fasta file saved: {filtered_concatenated_fasta_file}")
    
    # Increment the processed_samples_count for each processed sample
    processed_samples_count += 1

    # Define the path to the summary report created in Module 01
    summary_report_path_module_01 = os.path.join(args.input, '01_GENOMAD', str(sample_name), str(sample_name) + '_MVP_01_Summary_Report.txt')

    # Open the summary report created in Module 01 and read its content
    with open(summary_report_path_module_01, 'r') as module_01_summary_report:
        module_01_summary_content = module_01_summary_report.read()

    # Define the additional lines you want to add to the summary report
    module_02_lines_to_add = """
****************************************************************************
******************               MODULE 02                ******************
****************************************************************************
"""

    # Create a dictionary to hold argument descriptions and their default values
    argument_defaults = {
        '--input': args.input,
        '--metadata': args.metadata,
        '--sample-group': args.sample_group,
        '--viral-min-genes': args.viral_min_genes,
        '--host-viral-genes-ratio': args.host_viral_genes_ratio}

    # Write a summary line with script arguments and their default values
    summary_line = "02_filter_genomad_checkv.py"
    for arg, default in argument_defaults.items():
        if default is not None:
            summary_line += f" {arg} {default}"

    # Combine the content of Module 01 and Module 02 lines
    complete_summary_content = module_01_summary_content + module_02_lines_to_add + summary_line

    # Define the path to the summary report for Module 02
    summary_report_path_module_02 = os.path.join(args.input, '02_CHECK_V', str(sample_name), str(sample_name) + '_MVP_02_Summary_Report.txt')

    # Write the combined content to the new summary report for Module 02
    with open(summary_report_path_module_02, 'w') as module_02_summary_report:
        module_02_summary_report.write(complete_summary_content)

    print("Summary report for Module 02 has been created: ", summary_report_path_module_02)

message1 = "\n\033[1mModule 02 finished: output summary files merged and filtered successfully!\033[0m\n"
message2 = "Genomad_CheckV_Virus_Proviruses_Quality_Summary.tsv and viruses_proviruses.fna files have been generated for each sample in their respective directories in 02_CHECK_V."
message3 = "viruses_proviruses.fna files will be used to run Module 03 (clustering)."
message4 = "\n\033[1mYou can now proceed to the next step of the MVP script: Module 03!\033[0m\n"
line_of_stars = '*' * len(message2)
print()
print(line_of_stars)
print(message1)
print(message2)
print(message3)
print(message4)
print(line_of_stars)
print()