import argparse
import glob
import os
import subprocess
import sys
from os.path import getsize
import pandas as pd
from functools import reduce
from Bio import SeqIO

# Parse command line arguments
parser = argparse.ArgumentParser(description='Module 01: Run geNomad and Check_V on multiple nucleotide files.')
parser.add_argument('-i', '--input', help='Path to your working directory.', required=True)
parser.add_argument('-m', '--metadata', help='Path to your metadata file.', required=True)
parser.add_argument('--sample-group', help='Specific sample number(s) to run the script on (can be a comma-separated list: 1,2,6 for example). By default, MVP processes all datasets listed in the metadata file one after the other.', type=str, default='')
parser.add_argument('--modify-headers', help='Modify sequence headers by adding sample name', action='store_true')
parser.add_argument('--min-seq-size', help='Minimum sequence size to keep (in base pairs)', type=int, default=0)
parser.add_argument('--min-score', help='Minimum score for geNomad filtering', default=0.7, type=float)
parser.add_argument('--genomad-db-path', help='Path to the geNomad database directory.', default='')
parser.add_argument('--checkv-db-path', help='Path to the CheckV database directory.', default='')
parser.add_argument('--force-genomad', help='Run geNomad even if output already exists.', action='store_true')
parser.add_argument('--force-checkv', help='Run CheckV even if output already exists.', action='store_true')
parser.add_argument('--threads', type=int, default=1, help='Number of threads to use (default = 1)')

args = parser.parse_args()

# read metadata file and map FASTQ files
metadata = pd.read_csv(args.metadata, sep='\t')

# Create a new directory for modified and filtered assembly files
if args.modify_headers or args.min_seq_size:
    modified_assembly_dir = os.path.join(args.input, '00_MODIFIED_ASSEMBLY_FILES')
    os.makedirs(modified_assembly_dir, exist_ok=True)

# Determine the geNomad database path
genomad_db_path = os.path.join(args.input, '01_GENOMAD', 'genomad_db')
if args.genomad_db_path:
    genomad_db_path = args.genomad_db_path

# Determine the checkV database path
checkv_db_path = os.path.join(args.input, '02_CHECK_V', 'checkv-db-v1.5')
if args.checkv_db_path:
    checkv_db_path = args.checkv_db_path

# Process rows based on specific sample numbers or all rows
specific_sample_numbers = []
if args.sample_group:
    specific_sample_numbers = [int(num) for num in args.sample_group.split(',') if num.strip()]

# Initialize a counter for processed samples
processed_samples_count = 0

for row_index, (_, row) in enumerate(metadata.iterrows(), start=1):
    input_assembly_file = row['Assembly_Path']
    sample_number = row['Sample_number']
    sample_name = row['Sample']

    # Check if the current sample number is in the list of specific sample numbers
    if specific_sample_numbers and sample_number not in specific_sample_numbers:
        continue # Skip to the next iteration

    # Extract the directory path from the input assembly file
    input_directory = os.path.dirname(input_assembly_file)

    # Construct the new output file name in the modified assembly directory
    output_filename = f'{sample_name}_modified.fna'
    if args.modify_headers or args.min_seq_size:
        output_assembly_file = os.path.join(modified_assembly_dir, output_filename)
    else:
        output_assembly_file = os.path.join(input_directory, output_filename)

    # Read sequences, modify headers, filter by size, and write to the new file
    sequences = []
    with open(input_assembly_file, "r") as input_fasta:
        for record in SeqIO.parse(input_fasta, "fasta"):
            if args.modify_headers:
                # Modify the header by adding the sample name at the beginning
                new_header = f"{sample_name}_{record.id}"
                record.id = new_header
                record.description = new_header

            if len(record.seq) > args.min_seq_size:
                sequences.append(record)

    # Write the modified and filtered sequences to the new file
    if args.modify_headers or args.min_seq_size:
        with open(output_assembly_file, "w") as output_fasta:
            SeqIO.write(sequences, output_fasta, 'fasta-2line')

        # Use the output_assembly_file for Genomad if it was modified
        input_for_genomad = output_assembly_file
    else:
        input_for_genomad = input_assembly_file

    # Create directories for the current sample in 01_GENOMAD and 02_CHECK_V
    genomad_sample_directory = os.path.join(args.input, '01_GENOMAD', str(sample_name))
    os.makedirs(genomad_sample_directory, exist_ok=True)
    checkv_sample_directory = os.path.join(args.input, '02_CHECK_V', str(sample_name))
    os.makedirs(checkv_sample_directory, exist_ok=True)

    # Run Genomad
    genomad_output = os.path.join(genomad_sample_directory, str(sample_name) + '_Viruses_Genomad_Output')
    if os.path.exists(genomad_output) and not args.force_genomad:
        print(f'Skipping geNomad for {sample_name}, {genomad_output} folder already exists')
    else:
        print(f'\nRunning geNomad on {sample_name}...\n')
        subprocess.run(['genomad', 'end-to-end', '--min-score', str(args.min_score), '--cleanup', '--splits', '8', '-t', str(args.threads), input_for_genomad, genomad_output, genomad_db_path])

    # Run Check_V
    checkv_input_files = glob.glob(os.path.join(genomad_output, '**/*_virus.fna'))
    checkv_input = checkv_input_files[0]
    checkv_output = os.path.join(checkv_sample_directory, str(sample_name) + '_Viruses_CheckV_Output')
    if os.path.exists(checkv_output) and not args.force_checkv:
        print(f"Skipping Check_V for {sample_name}, {checkv_output} folder already exists")
    else:
        if getsize(checkv_input) == 0:
            print(f'Skipping Check_V {checkv_input} because it is empty.')
            # Create output folder for skipped files
            empty_checkv_output = os.path.join(checkv_output)
            os.makedirs(empty_checkv_output, exist_ok=True)
            # Write skipped_files.txt in the skipped folder
            with open(os.path.join(empty_checkv_output, 'Virus_CheckV_logfile.txt'), 'a') as f:
                f.write('Creating empty virus.fna and quality_summary.tsv')
            # Create empty viruses.fna and proviruses.fna files
            with open(os.path.join(empty_checkv_output, 'viruses.fna'), 'w') as f:
                pass
            with open(os.path.join(empty_checkv_output, 'proviruses.fna'), 'w') as f:
                pass
            with open(os.path.join(empty_checkv_output, 'quality_summary.tsv'), 'w') as f:
                f.write('contig_id\tcontig_length\tprovirus\tproviral_length\tgene_count\tviral_genes\thost_genes\tcheckv_quality\tmiuvig_quality\tcompleteness\tcompleteness_method\tcontamination\tkmer_freq\twarnings\n')
        else:
            print(f'\nRunning Check_V on {sample_name}...\n')
            subprocess.run(['checkv', 'end_to_end', checkv_input, checkv_output, '-d', checkv_db_path, '-t', str(args.threads), '--remove_tmp'])
            genomad_proviruses_input = os.path.join(checkv_output, 'proviruses.fna')
            new_genomad_proviruses_input = genomad_proviruses_input + '.new'
            with open(genomad_proviruses_input, 'r') as f_in, open(new_genomad_proviruses_input, 'w') as f_out:
                for line in f_in:
                    if line.startswith('>'):
                        new_header = line.strip().replace(' ', '|')
                        f_out.write(new_header + '\n')
                    else:
                        f_out.write(line)
            # Replace the original file with the modified file
            os.replace(new_genomad_proviruses_input, genomad_proviruses_input)

    # Run Genomad on proviruses
    genomad_proviruses_input = os.path.join(checkv_output, 'proviruses.fna')
    genomad_proviruses_output = os.path.join(genomad_sample_directory, str(sample_name) + '_Proviruses_Genomad_Output')
    if os.path.exists(genomad_proviruses_output) and not args.force_genomad:
        print(f'Skipping geNomad on proviruses for {sample_name}, {genomad_proviruses_output} folder already exists')
    else:
        if getsize(genomad_proviruses_input) == 0:
            print(f'Skipping geNomad on proviruses {genomad_proviruses_input} because it is empty.')
            # Create output folder for skipped files
            empty_genomad_proviruses_output = os.path.join(genomad_proviruses_output, 'proviruses_summary')
            os.makedirs(empty_genomad_proviruses_output, exist_ok=True)
            # Write skipped_files.txt in the skipped folder
            with open(os.path.join(empty_genomad_proviruses_output, 'Provirus_Genomad_logfile.txt'), 'a') as f:
                f.write('Creating empty proviruses_virus.fna and proviruses_virus_summary.tsv')
            # Create empty viruses.fna file
            with open(os.path.join(empty_genomad_proviruses_output, 'proviruses_virus.fna'), 'w') as f:
                pass
            with open(os.path.join(empty_genomad_proviruses_output, 'proviruses_virus_summary.tsv'), 'w') as f:
                f.write('seq_name\tlength\ttopology\tcoordinates\tn_genes\tgenetic_code\tvirus_score\tfdr\tn_hallmarks\tmarker_enrichment\ttaxonomy\n')
        else:
            print(f'\nRunning geNomad on proviruses {sample_name}...')
            subprocess.run(['genomad', 'end-to-end', '--min-score', '0.5', '--cleanup', '--splits', '8', '-t', str(args.threads), genomad_proviruses_input, genomad_proviruses_output, genomad_db_path])

    # Run Check_V on proviruses
    checkv_proviruses_input = os.path.join(genomad_proviruses_output, 'proviruses_summary', 'proviruses_virus.fna')
    checkv_proviruses_output = os.path.join(checkv_sample_directory, str(sample_name) + '_Proviruses_CheckV_Output')
    if os.path.exists(checkv_proviruses_output) and not args.force_checkv:
        print(f'Skipping Check_V on proviruses for {sample_name}, {checkv_proviruses_output} folder already exists')
    else:
        if getsize(checkv_proviruses_input) == 0:
            print(f'Skipping Check_V on {checkv_proviruses_input} because it is empty.')
            # Create output folder for skipped files
            os.makedirs(checkv_proviruses_output, exist_ok=True)
            # Write skipped_files.txt in the skipped folder
            with open(os.path.join(checkv_proviruses_output, 'Provirus_CheckV_logfile.txt'), 'a') as f:
                f.write('Creating empty viruses.fna and quality_summary.tsv')
            # Create empty viruses.fna file
            with open(os.path.join(checkv_proviruses_output, 'viruses.fna'), 'w') as f:
                pass
            with open(os.path.join(checkv_proviruses_output, 'quality_summary.tsv'), 'w') as f:
                f.write('contig_id\tcontig_length\tprovirus\tproviral_length\tgene_count\tviral_genes\thost_genes\tcheckv_quality\tmiuvig_quality\tcompleteness\tcompleteness_method\tcontamination\tkmer_freq\twarnings\n')
        else:
            print(f'\nRunning Check_V on proviruses {sample_name}...')
            subprocess.run(['checkv', 'end_to_end', checkv_proviruses_input, checkv_proviruses_output, '-d', checkv_db_path, '-t', str(args.threads), '--remove_tmp'])

        # Open the quality summary file for reading
        checkv_proviruses_output_file = os.path.join(checkv_proviruses_output, 'quality_summary.tsv')
        with open(checkv_proviruses_output_file, 'r') as f:
            lines = f.readlines()

        # Find the index of the 'provirus' column
        header = lines[0].strip().split('\t')
        provirus_col_index = header.index('provirus')

        # Modify the lines where provirus is 'No' to 'Yes'
        for i in range(1, len(lines)):
            fields = lines[i].strip().split('\t')
            if fields[provirus_col_index] == 'No':
                fields[provirus_col_index] = 'Yes'
                lines[i] = '\t'.join(fields) + '\n'

        # Write the modified lines to the original file
        with open(checkv_proviruses_output_file, 'w') as f:
            f.writelines(lines)
    
    # Increment the processed_samples_count for each processed sample
    processed_samples_count += 1

    # Specify the path to the existing MVP_Summary_Report.txt
    summary_report_path = os.path.join(args.input, 'MVP_00_Summary_Report.txt')

    # Read the existing content of the summary report
    with open(summary_report_path, 'r') as existing_summary_report:
        existing_content = existing_summary_report.read()

    # Define the lines you want to add
    module_header = """
****************************************************************************
******************               MODULE 01                ******************
****************************************************************************
"""

    # Create a dictionary to hold argument descriptions and their default values
    argument_defaults = {
        '--input': args.input,
        '--metadata': args.metadata,
        '--min-score': args.min_score,
        '--sample-group': args.sample_group,
        '--modify-headers': args.modify_headers,
        '--min-seq-size': args.min_seq_size,
        '--threads': args.threads}

    # Write a summary line with script arguments and their default values
    summary_line = "01_run_genomad_checkv.py"
    for arg, default in argument_defaults.items():
        if default is not None:
            summary_line += f" {arg} {default}"

    # Append the new summary_line to the existing content
    modified_content = existing_content + module_header + summary_line

    # Specify the path for the new MVP_Summary_Report.txt in '01_GENOMAD' directory
    summary_report_path = os.path.join(genomad_sample_directory, str(sample_name) + '_MVP_01_Summary_Report.txt')

    # Write the modified content to the new summary report in '01_GENOMAD'
    with open(summary_report_path, 'w') as modified_summary_report:
        modified_summary_report.write(modified_content)

    print("Summary report for Module 01 has been created: ", summary_report_path, " in ", genomad_sample_directory)

message1 = "\n\033[1mModule 01 finished: geNomad and CheckV executed successfully!\033[0m\n"
message2 = "geNomad and CheckV output directories for both viruses and proviruses have been generated."
message3 = "You can look in <sample_name>_virus_summary.tsv, proviruses_virus_summary.tsv, and quality_summary.tsv tabular files for a summary of geNomad and Check_V outputs."
message4 = "\n\033[1mYou can now proceed to the next step of the MVP script: Module 02!\033[0m\n"
line_of_stars = '*' * len(message2)
print()
print(line_of_stars)
print(message1)
print(message2)
print(message3)
print(message4)
print(line_of_stars)
print()

print("Please don't forget to cite the following softwares used by this module:")
print("- Camargo, A. P., Roux, S., Schulz, F., Babinski, M., Xu, Y., Hu, B., Chain, P. S. G., Nayfach, S., & Kyrpides, N. C. You can move, but you can’t hide: identification of mobile genetic elements with geNomad. bioRxiv (2023), DOI: 10.1101/2020.11.01.361691")
print("\n- Nayfach, S., Camargo, A.P., Schulz, F. et al. CheckV assesses the quality and completeness of metagenome-assembled viral genomes. Nat Biotechnol 39, 578–585 (2021). https://doi.org/10.1038/s41587-020-00774-7\n")