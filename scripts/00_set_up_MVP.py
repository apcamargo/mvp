import argparse
import glob
import os
import subprocess
import sys
from os.path import getsize
import pandas as pd
from functools import reduce
from Bio import SeqIO

# Parse command line arguments
parser = argparse.ArgumentParser(description='Module 00: check your metadata, your assembly and read files, creates all the repositories, and install the required databases.')
parser.add_argument('-i', '--input', help='Path to your working directory where you want to run MVP.', required=True)
parser.add_argument('-m', '--metadata', help='Path to your metadata file.', required=True)
parser.add_argument('--install-databases', help='Install geNomad and checK databases in the respective repositories. (default = false)', action='store_true')
args = parser.parse_args()

# read metadata file and map FASTQ files
metadata = pd.read_csv(args.metadata, sep='\t')

# Convert metadata file to Unix line endings using dos2unix
subprocess.run(['dos2unix', args.metadata])

# Verify metadata and perform checks
print('\nChecking your metadata...\n')
required_columns = ['Sample', 'Assembly_Path', 'Read_Path']
missing_columns = [col for col in required_columns if col not in metadata.columns]

# Check if all columns have the same number of rows
num_rows = metadata.shape[0]
column_counts = metadata.apply(lambda col: col.count())
columns_with_empty_cells = metadata.columns[metadata.isnull().any()]

if missing_columns or not columns_with_empty_cells.empty:
    if missing_columns:
        print(f"Error: The following required columns are missing in the metadata file: {', '.join(missing_columns)}")
    if not columns_with_empty_cells.empty:
        print(f"Error: The following columns have empty cells: {', '.join(columns_with_empty_cells)}")
    sys.exit(1)

# Check if Assembly_Path and/or Read_Path correspond to existing files
print('Checking the paths of your sequencing data...\n')
nonexistent_assembly_paths = metadata.loc[~metadata['Assembly_Path'].apply(lambda path: path == '' or (isinstance(path, str) and os.path.exists(path))), 'Assembly_Path']
nonexistent_read_paths = metadata.loc[~metadata['Read_Path'].apply(lambda path: path == '' or (isinstance(path, str) and os.path.exists(path))), 'Read_Path']

if not nonexistent_assembly_paths.empty or not nonexistent_read_paths.empty:
    if not nonexistent_assembly_paths.empty:
        print(f"Error: The following Assembly_Path values correspond to non-existent files: {', '.join(nonexistent_assembly_paths)}")
    if not nonexistent_read_paths.empty:
        nonexistent_read_paths_str = nonexistent_read_paths.astype(str)
        print(f"Error: The following Read_Path values correspond to non-existent files: {', '.join(nonexistent_read_paths_str)}")
        sys.exit(1)

# Process rows based on specific row numbers or all rows
print('Checking potential errors in your sequencing data...\n')
for row_index, (_, row) in enumerate(metadata.iterrows(), start=1):
    input_assembly_file = row['Assembly_Path']
    sample_name = row['Sample']
    
    # Verify that the input assembly file contains only valid nucleotide characters and does not have duplicate headers
    with open(input_assembly_file, "r") as input_fasta:
        headers_seen = set()
        invalid_chars_found = False
        duplicate_headers_found = False
        
        for record in SeqIO.parse(input_fasta, "fasta"):
            header = record.id
            if any(base not in "ACTGNactgn" for base in record.seq):
                print(f"Error: Invalid nucleotide characters found in {input_assembly_file} for sequence {record.id}\n")
                invalid_chars_found = True
            if header in headers_seen:
                print(f"Error: Duplicate header found in {input_assembly_file}: {header}\n")
                duplicate_headers_found = True
            headers_seen.add(header)
        
        if invalid_chars_found or duplicate_headers_found:
            sys.exit(1)

# Create required output folders
print('Creating MVP depositories...')
output_folders = ['01_GENOMAD', '02_CHECK_V', '03_CLUSTERING', '04_READ_MAPPING', '05_VOTU_TABLES', '06_FUNCTIONAL_ANNOTATION']
for folder_name in output_folders:
    output_folder = os.path.join(args.input, folder_name)
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

# Install databases if requested
if args.install_databases:
    print("\nInstalling geNomad and CheckV databases...\n")

    # Install geNomad database in '01_GENOMAD' folder
    genomad_db_path = os.path.join(args.input, '01_GENOMAD', 'genomad_db')
    if os.path.exists(genomad_db_path):
        print('Skipping to create geNomad database, genomad_db already exists.')
    else:
        subprocess.run(['genomad', 'download-database', os.path.join(args.input, '01_GENOMAD')])

    # Install CheckV database in '02_CHECK_V' folder
    checkv_db_path = os.path.join(args.input, '02_CHECK_V', 'checkv-db-v1.5')
    if os.path.exists(checkv_db_path):
        print('Skipping to create Check_V database, checkv-db-v1.5 already exists.')
    else:
        subprocess.run(['checkv', 'download_database', os.path.join(args.input, '02_CHECK_V')])

# Create a dictionary to hold argument descriptions and their default values
argument_defaults = {
    '--input': args.input,
    '--metadata': args.metadata,
    '--install-databases': args.install_databases}

# Create the module header
module_header = """****************************************************************************
*********                          MODULE 00                       *********
****************************************************************************
"""

# Write a summary line with script arguments and their default values
summary_line = "00_set_up_mvp.py"
for arg, default in argument_defaults.items():
    if default is not None:
        summary_line += f" {arg} {default}"

# Create a summary report file
summary_report_path = os.path.join(args.input, 'MVP_00_Summary_Report.txt')

with open(summary_report_path, 'w') as summary_report:
    # Write the module header
    summary_report.write(module_header)

    # Write the summary line
    summary_report.write(summary_line + '\n\n')
    genomad_db_path = os.path.join(args.input, '01_GENOMAD', 'genomad_db')
    checkv_db_path = os.path.join(args.input, '02_CHECK_V', 'checkv-db-v1.5')
    summary_report.write(f"- geNomad database path: {genomad_db_path}\n")
    summary_report.write(f"- CheckV database path: {checkv_db_path}\n")

print("Summary report for Module 00 has been created: ", summary_report_path)

message1 = "\n\033[1mModule 00 finished: you are all set to run MVP!\033[0m\n"
message2 = "Your metadata looks fine, as well as your read and assembly files."
message3 = "All the directories needed for for MVP have been generated in your working directory."
if args.install_databases:
    message4 = "geNomad and CheckV databases has been installed in their respective directories."
message5 = "\n\033[1mYou can now proceed to the next step of the MVP script: Module 01!\033[0m\n"
line_of_stars = '*' * len(message2)
print()
print(line_of_stars)
print(message1)
print(message2)
print(message3)
if args.install_databases:
    print(message4)
print(message5)
print(line_of_stars)
print()

print("Please don't forget to cite the following softwares used by this module:")
print("- Camargo, A. P., Roux, S., Schulz, F., Babinski, M., Xu, Y., Hu, B., Chain, P. S. G., Nayfach, S., & Kyrpides, N. C. You can move, but you can’t hide: identification of mobile genetic elements with geNomad. bioRxiv (2023), DOI: 10.1101/2020.11.01.361691")
print("\n- Nayfach, S., Camargo, A.P., Schulz, F. et al. CheckV assesses the quality and completeness of metagenome-assembled viral genomes. Nat Biotechnol 39, 578–585 (2021). https://doi.org/10.1038/s41587-020-00774-7\n")
