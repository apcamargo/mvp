import os
import argparse
import subprocess
import pandas as pd
import shutil

# Parse command line arguments
parser = argparse.ArgumentParser(description='Module 03: Viral sequence clustering and reference preparation for the read mapping step.')
parser.add_argument('-i', '--input', help='Path to your working directory.', required=True)
parser.add_argument('-m', '--metadata', help='Input metadata containing sample names, nucleotide file paths, and read files.', required=True)

