![MVP_Logo.pdf](images/MVP_Logo.png){width:150px;height:100px;}

# **MVP v.1.0: Multi-choice Viromics Pipeline**
## Visuals

## QUICK LINKS
[Overview](https://gitlab.com/ccoclet/mvp/-/blob/main/README.md#overview)  
[Databases and versions](https://gitlab.com/ccoclet/mvp/-/blob/main/README.md#databases-and-versions)  
[Installation](https://gitlab.com/ccoclet/mvp/-/blob/main/README.md#installation)  
[Quick start](https://gitlab.com/ccoclet/mvp/-/blob/main/README.md#quick-start)  
[Main output files](https://gitlab.com/ccoclet/mvp/-/blob/main/README.md#main-output-files)  
[References](https://gitlab.com/ccoclet/mvp/-/blob/main/README.md#references)  
[Contributing](https://gitlab.com/ccoclet/mvp/-/blob/main/README.md#contributing)  
[License / Copyright](https://gitlab.com/ccoclet/mvp/-/blob/main/README.md#license-copyright)

## OVERVIEW
**MVP** stands for **M**ulti-choice **V**iromics **P**ipeline. It is a simplified pipeline that utilizes a suite of state-of-art tools to easily get from a set of contigs to a vOTU heatmap (and more):
1. **geNomad** to identify viruses, proviruses, and plasmids in sequencing data,
1. **CheckV** to assess the quality, and completeness of identified viral genomes, including identification of host contamination for integrated proviruses, 
1. A custom code for a rapid genome clustering based on pairwise ANI (also provided with **CheckV**),
1. **Bowtie2, Samtools, and CoverM** to calculate coverage of individual viral OTUs by read mapping,
1. A custom code to create a vOTU table of abundance,
1. **MMseqs2** to compare viral proteins to multiple databases.

It is a quick and intuitive way to get a list of viral sequences and their properties that can be used for downstream analyses. The main contributors of each software used in MVP should be acknowledged (Citations and links are provided):  

![MVP_Workflow.pdf](images/MVP_Simplified_Workflow.png){width:150px;height:100px;}

Link to [**geNomad github**](https://github.com/apcamargo/genomad)  
[**Camargo, A. P.**, Roux, S., Schulz, F., Babinski, M., Xu, Y., Hu, B., Chain, P. S. G., Nayfach, S., & Kyrpides, N. C. **You can move, but you can’t hide: identification of mobile genetic elements with geNomad.** bioRxiv (2023), DOI: 10.1101/2020.11.01.361691](https://www.biorxiv.org/content/10.1101/2023.03.05.531206v1)  

Link to [**CheckV github**](https://bitbucket.org/berkeleylab/checkv/src/master/)  
[**Nayfach, S.**, Camargo, A.P., Schulz, F. et al. **CheckV assesses the quality and completeness of metagenome-assembled viral genomes.** Nat Biotechnol 39, 578–585 (2021). https://doi.org/10.1038/s41587-020-00774-7](https://www.nature.com/articles/s41587-020-00774-7)  

Link to [**Bowtie2 github**](https://github.com/BenLangmead/bowtie2)  
[**Langmead B**, Salzberg S. **Fast gapped-read alignment with Bowtie 2.** Nature Methods. 2012, 9:357-359](https://www.nature.com/articles/nmeth.1923)  

Link to [**minimap2**](https://github.com/lh3/minimap2)  
[**Li H., New strategies to improve minimap2 alignment accuracy. Bioinformatics.** 2021, 37(23):4572–4574.](https://academic.oup.com/bioinformatics/article/37/23/4572/6384570?login=true)  

Link to [**Samtools github**](https://github.com/samtools/samtools)  
[**Li, H.**, B. Handsaker, A. Wysoker, T. Fennell, J. Ruan, N. Homer, G. Marth, G. Abecasis, R. Durbin, and 1000 Genome Project Data Processing Subgroup. **The Sequence Alignment/Map Format and SAMtools.** Bioinformatics 25, no. 16 2009: 2078–79.](https://academic.oup.com/bioinformatics/article/25/16/2078/204688?login=true)  

Link to [**CoverM github**](https://github.com/wwood/CoverM)  

Link to [**MMseqs2 github**](https://github.com/soedinglab/MMseqs2)  
[**Mirdita M**, Steinegger M, Breitwieser F, Soding J, Levy Karin E. **Fast and sensitive taxonomic assignment to metagenomic contigs.** Bioinformatics, doi: 10.1093/bioinformatics/btab184 (2021).](https://academic.oup.com/bioinformatics/article/37/18/3029/6178277)  

## INSTALLATION
### Conda installation

### Docker installation

### Installing the latest version of MVP from this repository
- First, open your terminal and clone the MVP repository to get the latest version of the scripts
```bash
git clone https://gitlab.com/ccoclet/mvp.git
```
- Next, create the corresponding conda environment and install the required packages  
```bash
cd mvp
conda env create -f mvp_environment.yaml
#OR
mamba env create -f mvp_environment.yaml
```
If you work on MacOS, you need to use mvp_mac_environment.yaml as CoverM is not available currently for MacOS systems
```bash
conda env create -f mvp_mac_environment.yaml
#OR
mamba env create -f mvp_mac_environment.yaml
```
- To activate this environment, use
```bash
conda activate mvp
```

## DATABASES, METADATA, AND DIRECTORIES
The latest versions of geNomad and CheckV databases will be downloaded when you run the first command of the script, if you include the ```--install-databases``` argument. MVP will skip these steps if the databases already exist.  

To start using MVP, you will need input files and directories:  
- a **working directory** (often named based on your project name) where MVP will create the output files,  
- a **metadata** (.txt) with 4 columns: Sample_number (sample group number), Sample (sample name), Assemply_Path (the absolute path of your input sequencing data files), Read_Path (the absolute path of your read files .fastq or .fastq.gz) (see directory data below for the content of each column),  
- your **sequencing files** (.fna, .fasta, or .fa) in a directory readable by MVP (in the example below, we will use a directory called **00_ASSEMBLY_FILES**),  
- your **read files** (.fastq, .fastq.gz) in a directory reable by MVP, if you want to run Modules 04 and 05 (read mapping and abundance table). In the example below, we will use a directory called **00_READ_FILES**.

*Notes:*  
*(1) If you run MVP for multiple projects and want to keep track of the input/output, we encourage you to save your metadata files and/or your sequencing and read files in directories such as **00_ASSEMBLY_FILES**, and **00_READ_FILES** in the working directory.*  
*(2) If you have forward and reverse reads split into R1 and R2 files, you can just provide the forward R1 read file path in the column **Read_Path** of your metadata. If not already present, be sure to include **```R1```** and **```R2```** in the name of your forward R1 and reverse R2 read files. The script will find the path of the reverse R2 read file by itself if it is identical to the R1 file with just R1 changed to R2.*  

## RUNNING THE PIPELINE - NORMAL MODE
### Executing Module 00
```python
python scripts/00_set_up_mvp.py -h
python scripts/00_set_up_mvp.py -i WORKING_DIRECTORY/ -m data/example_metadata.txt --install-databases
```
#### Flag explanations
- ```-i``` (required) path to your working directory where all the MVP outputs will be generated and stored.  
- ```-m``` (required) your metadata file.  
- ```--install-databases``` argument to use if you haven't already installed geNomad and CheckV databases, or want to reinstall them (turned off by default).   

#### Module and output explanations
This module will first check for any potential errors/issues in your metadata and your sequencing/read files:  
- missing or extra columns, wrong column names,  
- empty cells,  
- wrong (i.e. unreadable) paths to your sequencing or read files,  
- any potential sequence errors (any characters different than A, C, T, G, or N),  
- duplicate hearders.  

Once your metadata has been checked, the module will create all the directories that MVP needs: **01_GENOMAD**, **02_CHECK_V**, **03_CLUSTERING**, **04_READ_MAPPING**, **05_VOTU_TABLES** and **06_FUNCTIONAL_ANNOTATION**.  
Finally, if you use the ```--install-databases``` argument, the module will install geNomad and checkV databases in **01_GENOMAD** and **02_CHECK_V**, respectively.

*Notes:*  
*(1) We highly encourage you to make sure that the geNomad and checkV databases are available on your system before deciding if you want to use ```--install-databases``` or not. If you decide to not use the argument while the databases are not installed, you won't have any errors at this step, however the Module 01 won't work.*  


### Executing Module 01
```python
python scripts/01_run_genomad_checkv.py -h
python scripts/01_run_genomad_checkv.py -i WORKING_DIRECTORY/ -m data/example_metadata.txt --min-score 0.7
```
#### Flag explanations
- ```-i``` (required) path to your working directory where all the MVP outputs will be generated and stored.  
- ```-m``` (required) your metadata file.  
- ```--sample-group``` specify one number or a list of numbers in the metadata file you want to process (by default, MVP will process all datasets listed in the metadata file one after the other).  
- ```--modify-headers``` if you want to modify the name of each sequence by prefixing with the sample name (default = false).  
- ```--min-seq-size``` if you want to filter each input file based on a minimum sequence length (in bp, default = 0).  
- ```--genomad-db-path``` and ```--checkv-db-path``` these optional arguments are used to provide the paths to geNomad and CheckV, respectively, if they were not installed with the script 00_set_up_mvp.py and are not located into **01_GENOMAD** and **02_CHECK_V**. If you used 00_set_up_mvp.py, then MVP will find the databases by itself and you don't need to specify anything.
- ```--min-score``` minimum cutoff applied to geNomad virus prediction score (default = 0.7).  
- ```--force-genomad``` and ```--force-checkv``` arguments force geNomad and/or CheckV execution even if the directories already exists (default = do not overwrite existing directories).  
- ```--threads``` set number of threads (default = 1).

#### Module and output explanations
This module will create sample directory, run two rounds of geNomad and CheckV on each sample and return results inside four directories into respective sample directory in **01_GENOMAD** and **02_CHECK_V**:
- **```<sample_name>```_Viruses_Genomad_Output**,  
- **```<sample_name>```_Viruses_CheckV_Output**,  
- **```<sample_name>```_Proviruses_Genomad_Output**,  
- **```<sample_name>```_Proviruses_CheckV_Output**.  

The **```<sample_name>```_virus_summary.tsv**, **proviruses_virus_summary.tsv**, and both **quality_summary.tsv** for viruses and proviruses tabular files summarize the results that were generated by the two rounds of geNomad and CheckV. These 4 files will be used to run Module 02.  

The second round of geNomad and CheckV is used to properly process proviruses trimmed by CheckV by computing a geNomad annotation and score only on the predicted provirus (instead of including the host contamination flagged by CheckV) and predict completeness with CheckV on this trimmed provirus. If none of the contigs was trimmed by the first round of CheckV, proviruses.fna file will be empty and empty files will be created for the second round of geNomad and CheckV.  

Summary reports **```<sample_name>```_MVP_01_Summary_Report.txt** are also generated and stored in the respective ```<sample_name>``` directories in **01_GENOMAD**.

*Notes:*  
*(1) This module will first create a new directory called **00_MODIFIED_ASSEMBLY_FILES** if you used the ```--modify-headers``` or ```--min-seq-size``` arguments, to store the modified assembly files.*  
*(2) We highly encourage using ```--modify-headers``` if some of your contigs across the different FASTA files may have identical names, if your FASTA files come from various analyses, or if the headers of your sequences do not have a consistent format.*  
*(2) MVP will skip geNomad and/or CheckV steps if the directories already exist and are not empty, unless you use ```--force-genomad``` and ```--force-checkv``` arguments.*  
*(3) You can check the Github repositories of [geNomad](https://github.com/apcamargo/genomad) and [CheckV](https://bitbucket.org/berkeleylab/checkv/src/master/) for more explanation on argument and output files.*  

### Executing Module 02
```python
python scripts/02_filter_genomad_checkv.py -h
python scripts/02_filter_genomad_checkv.py -i WORKING_DIRECTORY/ -m data/example_metadata.txt
```
#### Flag explanations
- ```-i``` (required) path to your working directory where all the MVP outputs will be generated and stored.  
- ```-m``` (required) your metadata file.  
- ```--sample-group``` specify one number or a list of numbers in the metadata file you want to process (by default, MVP will process all datasets listed in the metadata file one after the other).  
- ```--viral-min-genes``` minimum number of viral genes required to consider a virus prediction (based on CheckV annotation, default = 1).
- ```--host-viral-genes-ratio``` maximum ratio of host genes to viral genes required to consider a virus prediction (based on CheckV annotation, default = 1, i.e. no more host genes than viral genes).  

#### Module and output explanations
This module will merge and filter **```<sample_name>```_virus_summary.tsv**, **proviruses_virus_summary.tsv**, and both **quality_summary.tsv** for viruses and proviruses tabular files in the respective **```<sample_name>```_Viruses_CheckV_Output**. It will return to a single tabular file for each sample called **```<sample_name>```_Genomad_CheckV_Virus_Proviruses_Quality_Summary.tsv**. This tabular file lists all the viruses and proviruses that geNomad predicted and that passed the cutoffs, and gives you all geNomad and CheckV features (i.e. virus length, viral genes, completeness, taxonomy, etc.). 

Then the module will create a concatenated FASTA file for each sample of the identified virus and trimmed proviruses sequences called **```<sample_name>```_viruses_proviruses.fna**.  

Summary reports **```<sample_name>```_MVP_02_Summary_Report.txt** are also generated and stored in the respective ```<sample_name>``` directories in **02_CHECK_V**.  

*Notes:*  
*(1) You can choose to modify the ```--viral-min-genes``` and ```--host-viral-genes-ratio``` arguments at this step to filter your list of identified virus and proviruses sequences based on the number of viral genes. However, we recommend to keep these arguments at their default value in order to keep a comprehensive list of potential virus and proviruses at this step. You will be able to further filter your final tabular file using these arguments when you will run the Module 05.*

### Executing Module 03
```python
python scripts/03_do_clustering.py -h
python scripts/03_do_clustering.py -i WORKING_DIRECTORY/ -m data/example_metadata.txt
```
#### Flag explanations
- ```-i``` (required) path to your working directory where all the MVP outputs will be generated and stored.  
- ```-m``` (required) your metadata file.  
- ```--min_ani``` minimum ANI (Average Nucleotide Identity) value for clustering (default = 95).  
- ```--min_tcov``` minimum coverage (Aligned Fraction, or AF) of the target sequence (default = 85).  
- ```--min_qcov``` minimum coverage (Aligned Fraction, or AF) of the query sequence (default = 0).  
- ```--read-type``` sequencing data type (e.g. short vs long reads) (default = short)
- ```--threads``` set number of threads (default = 1).  

#### Module and output explanations
This module will first merge all the **```<sample_name>```_viruses_proviruses.fna** FASTA files generated by Module 02. It will return to a single FASTA file **All_Sample_Genomad_CheckV_Virus_Sequences.fna** in the respective sample directory in **03_CLUSTERING**.  

Then, the module will do a rapid genome clustering based on pairwise ANI, and return to 3 tabular files and 1 FASTA file:  
- **All_Sample_Genomad_CheckV_Virus_Representative_Sequences.fna**: a FASTA file containing all the representative sequences.  
- **All_Sample_Genomad_CheckV_Virus_Sequences_Summary.tsv**: all virus sequences with geNomad and CheckV features.  
- **All_Sample_Genomad_CheckV_Virus_Sequences_Clustering.tsv**: all-vs-all blastn of sequences.  
- **All_Sample_Genomad_CheckV_Virus_Sequences_Clustering_ANI.tsv**: pairwise ANI between sequence pairs.  
- **All_Sample_Genomad_CheckV_Virus_Sequences_Clustering_ANI_Clusters.tsv**: Results of a greedy clustering, using by default the MIUVIG recommended-parameters (95% ANI + 85% AF).  
- **All_Sample_Genomad_CheckV_Virus_Sequences_Clustering_ANI_Clusters_summary.tsv**: Results of a greedy clustering, using by default the MIUVIG recommended-parameters (95% ANI + 85% AF), along with all geNomad and CheckV information for each sequence/cluster.  

The module will generate a **MVP_03_summary_report.txt** file providing an overview of key statistics, quality assessments, and taxonomy distributions.  

The module will build an index using bowtie2-build (short reads) or minimap2 (long reads) from your FASTA file containing the representative sequences in the **04_READ_MAPPING** directory (in preparation of Module 04).  

Finally, the module will create 2 FASTA files containing protein sequences for both representative viruses and all viruses, respectively and geNomad functional annotation tables in **06_FUNCTIONAL_ANNOTATION**.  

*Notes:*  
*(1) **MVP_03_summary_report.txt** will be completed with read mapping information in Module 05.*  
*(2) bowtie2-build outputs a set of 6 files with suffixes ```<reference>.1.bt2```, ```<reference>.2.bt2```, ```<reference>.3.bt2```, ```<reference>.4.bt2```, ```<reference>.rev.1.bt2```, and ```<reference>.rev.2.bt2``` in **04_READ_MAPPING**. These files together constitute the index: they are all that is needed to align reads to that reference in Module 04.*  
*(3) If you work with long read data sequencing (e.g. PacBio, Oxford Nanpore, Illumina Complete Long reads), we recommand to use the argument ```--read-type long```. MVP will then use minimap2 to build the index (reference) needed in Module 04. It will return to a single file called ```reference.mmi``` in **04_READ_MAPPING**.

### Executing Module 04
```python
python scripts/04_do_read_mapping.py -h
python scripts/04_do_read_mapping.py -i WORKING_DIRECTORY/ -m data/example_metadata.txt --delete-files
```
#### Flag explanations
- ```-i``` (required) path to your working directory where all the MVP outputs will be generated and stored.  
- ```-m``` (required) your metadata file.  
- ```--sample-group``` specify one number or a list of numbers in the metadata file you want to process (by default, MVP will process all datasets listed in the metadata file one after the other).  
- ```--read-type``` sequencing data type (e.g. short vs long reads) (default = short)  
- ```--delete-files``` if you want MVP to delete all intermediary files once a sample is done.  
- ```--threads``` set number of threads (default = 1).  

#### Module and output explanations
This module will generate **```<sample_name>```.sam** by using bowtie2 (short reads) or minimap2 (long reads), and **```<sample_name>```.bam**, and **```<sample_name>```_sorted.bam** files by using bowtie2 to map reads from individual samples to the vOTU database generated in Module 03. Then, the module will use CoverM to calculate coverage based on read mapping, using the sorted BAM files sorted by reference, and return to one tabular file per sample : **```<sample_name>```_CoverM.csv** containing the different coverage measures for each viral sequences.  

*Notes:*  
*(1) You can check the Github repositories of [Bowtie2](https://github.com/BenLangmead/bowtie2), [minimap2](https://github.com/lh3/minimap2), [Samtools](https://github.com/samtools/samtools), and [CoverM](https://github.com/wwood/CoverM) for more explanation on argument and output files.*

### Executing Module 05
```python
python scripts/05_create_vOTU_table.py -h
python scripts/05_create_vOTU_table.py -i WORKING_DIRECTORY/ -m data/example_metadata.txt
```
#### Flag explanations
- ```-i``` (required) path to your working directory where all the MVP outputs will be generated and stored.  
- ```-m``` (required) your metadata file.  
- ```--covered-fraction``` minimum horizontal coverage fraction required to consider a coverage in the abundance tables. By default, MVP will output abundance tables with minimum covered fraction of 0.1, 0.5, and 0.9.
- ```--normalization``` which CoverM coverage metrics (```RPKM``` or ```FPKM```) you want for your abundance tables (default = RPKM).
- ```--filtration``` inclusion criteria you want to apply on the predicted viruses for your abundance tables. Can be  ```relaxed``` or ```conservative``` (default = conservative). These two levels are pre-defined combinations of minimum completeness, minimum viral genes, minimum length, and/or maximum host viral genes ratio (see below for a detailed explanation of each). Alternatively, custom cutoffs combinations can be applied using individual arguments below.
- ```--completeness``` minimum estimated completeness to include a viral genome in the abundance tables (default = 0).
- ```--viral-min-genes``` minimum number of viral genes required to include a virus prediction (based on CheckV annotation, default = 1).
- ```--viral_min_length``` minimum length required to include a virus prediction (default = 0).
- ```--host-viral-genes-ratio``` maximum ratio of host genes to viral genes required to include a virus prediction (default = 1).  

#### Module and output explanations
This module will merge all the **```<sample_name>```_CoverM.csv** tabular files to create an unfiltered viral OTU table and save it as **Unfiltered_RPKM_vOTU_table** in **05_VOTU_TABLES**. Then, the module will create a set of viral OTU tables based on the cutoffs (*i.e.*, horizontal coverage) and filtration mode (*i.e.*, conservative and relaxed) you choose. Finally, the module will complete the **summary_report.txt** generated in Module 03 with an overview of normalized abundance measures for vOTUs.

```--filtration``` argument:  
```conservative``` will apply the following cutoffs: only include viral sequences predicted as ≥50% complete by CheckV (AAI prediction) or viral sequences ≥5kb.  
```relaxed``` will include all viral sequences that were gathered and clustered in Module 03.  

*Notes:*  
*(1) We strongly encourage to run the Module 05 with ```--filtration conservative``` to filter your vOTU tables. The conservative mode will only keep Complete, High- and Medium-quality, and ≥5kb Low-quality vOTUs, and is usually a good default for most viral ecogenomics studies.*  
*(2) The last two argument (```--viral-min-genes``` and ```--host-viral-genes-ratio```) can also be used when you run Module 02. They are provided in Module 05 to allow the users to run only one round of clustering (Module 03) and read mapping (Module 04), and then test different cutoffs in Module 05 to evaluate their impacts on the final results.*

### Executing Module 06
```python
python scripts/06_do_functional_annotation.py -h
python scripts/06_do_functional_annotation.py -i WORKING_DIRECTORY/ -m data/example_metadata.txt
```
#### Flag explanations
- ```-i``` (required) path to your working directory where all the MVP outputs will be generated and stored.  
- ```-m``` (required) your metadata file.  
- ```--fasta-files``` Sequence and protein FASTA files (representative or all sequences) to use for functional annotation (Default = representative').  
- ```--PHROGS-evalue``` Significance e-value of match between target sequences and query (default = 0.01).  
- ```--PHROGS-score``` Score of match between target sequences and query (default = 60).  
- ```--PFAM-evalue``` Significance e-value of match between target sequences and query (default = 0.01).  
- ```--PFAM-score``` Score of match between target sequences and query (default = 50).  
- ```--ADS-evalue``` Significance e-value of match between target sequences and query (default = 0.01).  
- ```--ADS-score``` Score of match between target sequences and query (default = 60).  
- ```--ADS-seqid``` Sequence identity of match between target sequences and query (default = 30).  
- ```--RdRP``` if you want MVP to create the 07_RDRP_PHYLOGENY folder and search RdRP profiles.  
- ```--RdRP-evalue``` Significance e-value of match between target sequences and query (default = 0.01). 
- ```--RdRP-score``` Score of match between target sequences and query (default = 50).  
- ```--DRAM``` if you want MVP to create an input file to be process through DRAM-v.  
- ```--force``` argument force functional annotation even if functional annotation output files already exist.  
- ```--delete-files``` if you want MVP to delete all intermediary files once the functional annotation is done.  
- ```--threads``` set number of threads (default = 1).  

#### Module and output explanations
This module will use the FASTA file containing protein sequences generated by the Module 03 to search protein sequences against multiple databases (i.e. PHROGS, Pfam, and Anti-CRISPR) and return to unfiltered annotation tables in **06_FUNCTIONAL_ANNOTATION**.. Then, the module will filter all of these tables using score, e-value, and seuqence identity thresholds and merge all tables in a a single one will all functional annotation.  

If you use argument ```--RDRP```, the module will create a new folder **07_RDRP_PHYLOGENY**, search the protein sequences against RdRP HMM profiles, and return in multiple TSV files, notably ```07C_Filtered_Formatted_RdRP_Profile_Tab.tsv``` that can be used for RdRP phylogeny analyses, for example.  

If you use ```--DRAM```, the module will generate an input TSV file that can be used to run DRAM-v.  

Finally, the module will create a **summary_report.txt** generated with an overview of the functional annotation.  

*Notes:*  
*(1) We  encourage to run the Module 06 with ```--fasta-files representative```.*  


## RUNNING MVP FROM NMDC EDGE - IN DEVELOPMENT

## TO GO FURTHER
The R code below is just an example of how to visualize the data produced by the MVP. You can adapt and expand upon it to suit your specific research questions and data sets. First, it creates two essential visualizations: a heatmap and a non-metric multidimensional scaling (nMDS) plot. These visuals allow you to explore your metagenomic data in an intuitive and insightful way.  

The heatmap offers an overview of your data, making patterns, instantly recognizable. It uses log-transformed RPKM (Reads Per Kilobase of transcript per Million mapped reads) values to represent relative abundance of vOTUs. Tt incorporates metadata such as sample locations, genome types, and host types, using different colors to make the information more accessible.  

The nMDS plot, on the other hand, uses dimensionality reduction techniques to simplify complex data, making it easier to interpret.  

```python
source("data/log10_Function.R")
library(ComplexHeatmap)
library(vegan)

######################################################################################
#################                LOAD THE TABLES             #########################
######################################################################################
# Set the working directory
setwd("WORKING_DIRECTORY/")

# Read the metadata
metadata <- read.table("data/example_metadata.txt",h=T,sep=",",dec=".")
head(metadata)

# Read the vOTU Table
data <- read.table("05_VOTU_TABLES/Filtered_HC_0.1_RPKM_Representative_vOTU_Table.csv", h=T, sep=",", dec=".")
names(data)

######################################################################################
#################                     HEATMAP                #########################
######################################################################################
# Select the RPKM columns and create a new DataFrame with only the selected columns
selected_columns <- c(grep('^RPKM_', names(data), value = TRUE))
filtered_data <- data[selected_columns]
head(filtered_data)

# Rename columns to remove 'RPKM_' prefix
colnames(filtered_data) <- gsub('^RPKM_', '', colnames(filtered_data))
head(filtered_data)

# Create a matrix with log data
filtered_data_matrix <- as.matrix(filtered_data)
log_filtered_data_matrix <- log_10(filtered_data_matrix, lambda = 0)
head(log_filtered_data_matrix)

# Create annotation column
metadata$Sample <- factor(metadata$Sample)
annotation_col <- data.frame(Variable = metadata$Variable) # Variable factor can be any variable that you can use to vizualize differences between samples
log_filtered_data_matrix <- log_filtered_data_matrix[, match(metadata$Sample, colnames(log_filtered_data_matrix))]
head(log_filtered_data_matrix)

# Define annotation colors
ann_col_colors <- list(Variable = c(Variable_1 = "#98D8CF", Variable_2 = "#FDFFBA", Variable_3 = "#C6C3DF", Variable_4 = "#FC8A7F"),
                       Genome.type = c(dsDNA = "#C5002B", ssDNA = "#FA9946", Unknown = "#C6C6C6"),
                       Host.type = c(Prokaryote = "#499ECC", Eukaryote = "#7ECB7E", Unknown = "#C6C6C6"))

annotation_raws = data.frame(Genome.type = factor(data$Genome.type),
                             Host.type = factor(data$Host.type))

# Plot heatmap
plot <- pheatmap(log_filtered_data_matrix, name = "RPKM (Log10)",
                    annotation_col = annotation_col, main = "Samples",
                    annotation_colors = ann_col_colors, annotation_row = annotation_raws,
                    breaks=c(0,seq(0.1,16)), cluster_cols = F, 
                    col = c("#FFFFFF",colorRampPalette(c("#FDE333", "#E7D39A", "#F3935F", "#E54787", "#A8139C", "#4B1D91"))(16)))
plot

######################################################################################
#################                     nMDS                   #########################
######################################################################################
head(data)
head(metadata)

# Select the virus_id and RPKM columns and create a new DataFrame with only the selected columns
selected_columns <- c("virus_id", grep('^RPKM_', names(data), value = TRUE))
filtered_data <- data[selected_columns]
head(filtered_data)

# Rename columns to remove 'RPKM_' prefix
colnames(filtered_data) <- gsub('^RPKM_', '', colnames(filtered_data))
head(filtered_data)

# Transpose the dataframe
filtered_data_t <- dcast(melt(filtered_data, id.vars = "virus_id"), variable ~ virus_id)
filtered_data_t <- filtered_data_t %>% rename(Sample = variable)
head(filtered_data_t)

# Merge the transpose vOTU and the metadata tables
filtered_data_t_metadata <- merge(x = metadata, y = filtered_data_t, by = "Sample")

#Remove variables' columns to keep only RPKM columns
filtered_data_t_metadata_bio <- filtered_data_t_metadata[,-c(1:5)]

# Run nMDS
MDS <- cmdscale(vegdist(filtered_data_t_metadata_bio, method = "bray"), k = 3, eig = T, add = T )
round(MDS$eig*100/sum(MDS$eig),1)

filtered_data_t_metadata_bio_dist <- vegdist(filtered_data_t_metadata_bio, method = "bray")
filtered_data_t_metadata_bio_dist_MDS <- metaMDS(filtered_data_t_metadata_bio_dist, distance = "bray", k = 3, trymax=1000)

# Get the output values of the nMDS (*p-value*, R<sup>2</sup>, stress, eigen values)
adonis2(filtered_data_t_metadata_bio_dist~Location, method = "bray", permutations = 999, data = filtered_data_t_metadata)
pairwise.perm.manova(filtered_data_t_metadata_bio_dist, filtered_data_t_metadata$Location, nperm=999)

# Plot the nMDS
Variable <- factor(filtered_data_t_metadata$Variable, levels = c("Variable_1", "Variable_2", "Variable_3", "Variable_4"))
group <- factor(Variable)
color <- c("#98D8CF", "#FDFFBA", "#C6C3DF", "#FC8A7F")


par(mar=c(5, 4.5, 1, 6), xpd=T)
plot(filtered_data_t_metadata_bio_dist_MDS, type = "n", display = "sites", font.axis=2,
     cex.lab =1.3, cex.axis=1.3, font.lab = 2, xlab="nMDS 1 (34.0%)", ylab="nMDS 2 (18.5%)")
box(lwd=3)
points(filtered_data_t_metadata_bio_dist_MDS, display = "sites", pch = 21, col = "black", 
       bg = color[group], lwd = 2, cex = 3)
legend("topright", legend = levels(group), col= "black", pt.bg = color, pch=c(21), cex=1.5, pt.cex = 2.5, box.lwd=3)
text(-1,-1.5, "PERMANOVA (p-value = 0.001) \n Stress: 0.04 \n R2 = 0.66", font = 2)
```  

*Notes:*  
*(1) The code assumes that you have the required libraries (ComplexHeatmap and vegan) installed, as it uses functions from these libraries to create the heatmap and nMDS plot.*  

## COPYRIGHT NOTICE
Multi-choice Viromics Pipeline (MVP) Copyright (c) 2023, The Regents of the University of California, through Lawrence Berkeley National Laboratory (subject to receipt of any required approvals from the U.S. Dept. of Energy). All rights reserved.  

If you have questions about your rights to use or distribute this software, please contact Berkeley Lab's Intellectual Property Office at IPO@lbl.gov.  

NOTICE. This Software was developed under funding from the U.S. Department of Energy and the U.S. Government consequently retains certain rights.  As such, the U.S. Government has been granted for itself and others acting on its behalf a paid-up, nonexclusive, irrevocable, worldwide license in the Software to reproduce, distribute copies to the public, prepare derivative works, and perform publicly and display publicly, and to permit others to do so.  

## LICENCE AGREEMENT
GPL v3 License  

Multi-choice Viromics Pipeline (MVP) Copyright (c) 2023, The Regents of the University of California, through Lawrence Berkeley National Laboratory (subject to receipt of any required approvals from the U.S. Dept. of Energy). All rights reserved.  

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.  

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.